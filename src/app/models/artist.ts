export class artist {
  _id: string;
  name: string;
  image: string;
  createdAt: string;
  updatedAt: string;
}
