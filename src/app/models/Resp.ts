export class Resp {
  success: boolean;
  statusCode : any;
  message: string;
  data: any;
  count: number;
}
