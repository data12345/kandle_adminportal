export class album {
  _id: string;
  name: string;
  image: string;
  createdAt: string;
  updatedAt: string;
}
