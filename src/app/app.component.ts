import {Component, OnInit} from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';
import {ApiService} from './services/api/api.service';
import {ChatService} from './services/api/chat.service';
import Swal from 'sweetalert2';
import { Router, RouterModule } from '@angular/router';
import { CommonService } from './services/common/common.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
    providers: [ChatService]

})
export class AppComponent implements OnInit{
  title = 'KandleMusicAdminPortal';
  adminId:any;
  message:any;
  currentUrl:any;
  loginBody = {email: '', password: ''};
  constructor(
    private spinner: NgxSpinnerService,
    private ChatService: ChatService,
    private router: Router,
        private common: CommonService,

  ) {
  }
  ngOnInit() {
   if(localStorage.getItem('Kandle_Admin')){
    this.adminId = JSON.parse(localStorage.getItem('Kandle_Admin'))._value.id
    console.log(this.router.url,"url");
    console.log(this.currentUrl)
    console.log("storeid",this.adminId)
    if(this.adminId){
    this.ChatService.createBooking({'adminId':this.adminId}).subscribe((message: any) => {
        console.log('res',this.adminId,message)
        if (message) {
          console.log("admijn",message)
          this.message = message.message;
          this.common.successToast(this.message);
         // Swal.fire({
          //   position: 'top-end',
          //   title:"New message",
          //   text:  this.message,
          //   showConfirmButton: false,
          //   timer: 6000
          // })
        }
      });
   }
  }
  }

  show() {
    this.spinner.show();
  }
  hide() {
    this.spinner.hide();
  }
}
