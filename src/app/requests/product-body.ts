export class ProductBody {
  name: string;
  images: any;
  price: number;
  variantId:string='';
  Variant:string;
  productQuantity: number;
  purchaseQuantity: number;
  condition: string;
  category: string = '';
  subCategory: string = '';
  brand: string = '';
  searchKeyword: string;
  extraPrice: number;
  combo: any;
  comboDiscount: number;
  discount: number;
  description: string;
  id: string;
  options:any=[];
  subListItem: string='';
  listItem: any='';
  topDeals: boolean = false;
  isRecommended: boolean = false;
}
