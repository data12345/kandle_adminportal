export class AddUserBody {
  firstName: string;
  lastName: string;
  email: string;
  userName: string;
  phoneNo: string;
  countryCode:string=''
  latitude :any=0;
  longitude:any=0;
  // country: string=''
  state: string=''
  city: string;
  location: string;
  password: string;
  image:any;
  zipCode:string;
  streetName:string;
  profileType:string='';
  isNotification:boolean=false;
  isCommentAllow:boolean=false;;
  isLikeAllow:boolean=false;;
  id:string;


}
