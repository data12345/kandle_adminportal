export class ChangePasswordBody {
  oldPassword: string;
  newPassword: string;
  confirmPassword: string;
}
