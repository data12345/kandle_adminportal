export class Access {
  read: boolean = false;
  add: boolean = false;
  edit: boolean = false;
  delete: boolean = false;
}
export class AddSubAdminBody {
  firstName: string;
  lastName: string;
  email: string;
  state:string;
  zipCode:string;
  address:string;
  city:string;
  password: string;
  phoneNo: string;
  countryCode:string = '';
  adminId: string;
  role: string = 'SubAdmin';
  access: Access;
}
