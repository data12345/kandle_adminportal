export class AddSubCategoryBody {
  name: string;
  category: string;
  subCategory: string;
  id: string;
}
