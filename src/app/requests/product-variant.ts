export class ProductVariantBody {
  name: string;
  id:string;
  description:string;
  category: string = '';
  subCategory: string = '';
  options:any=[];
  subListItem: string='';
  listItem: any='';
}