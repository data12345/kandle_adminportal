import { Component, OnInit } from '@angular/core';
import * as js from '../../../assets/js/custom';
import { ApiService } from 'src/app/services/api/api.service';
import { Resp } from '../../models/Resp'
import * as _ from 'lodash';
// import {chartAreaDemo} from '../../../assets/js/custom';
import {Router} from '@angular/router';
import * as FusionCharts from 'fusioncharts';

const dataUrl =
'https://s3.eu-central-1.amazonaws.com/fusion.store/ft/data/line-chart-with-time-axis-data.json';
const schemaUrl =
'https://s3.eu-central-1.amazonaws.com/fusion.store/ft/schema/line-chart-with-time-axis-schema.json';
declare var window  :any 
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {
  dashboardList;
  dashboardGraph;
  dataSource: any;
  type: string;
  width: string;
  height: string;  role:any;
  access:any;
  constructor(private api: ApiService,private router: Router,
) { 
this.type = 'timeseries';
this.width = '80%';
this.height = '500';
this.dataSource = {
  data: null,
  chart: {
    showlegend: 0
  },
  navigator: {
    enabled: 0,
  },
  caption: {
    text: 'Revenue'
  },
  subcaption: {
    text: 'UserData'
  },
  yAxis: [
    {
      plot: {
        value: 'UserData',
        type: 'column'
      },
      format: {
        prefix: '$'
      },
      title: 'User'
    }
  ]
};
this.fetchData();
}

  ngOnInit() {
     if (JSON.stringify(localStorage.getItem('Kandle_Admin'))) {
      this.role = JSON.parse(localStorage.getItem('Kandle_Admin'))._value.role
      this.access = JSON.parse(localStorage.getItem('Kandle_Admin')).access
    }
    this.getDashboardData();
  }
  fetchData() {
    var dataFetch:any;
    var jsonify = res => res.json();
    var schemaFetch = 
    [{
      "name": "Time",
      "type": "date",
      // "format":"%y-%m-%d"
      "format": "%d-%b-%y"
    }, {
      "name": "UserData",
      "type": "number"
    }]
   
    this.api.getDahboardGraph({}).subscribe((response:any)=>{
      if (response.statusCode == 200) {
      dataFetch=response.data;
      // console.log("dataFetch",dataFetch)
       
     Promise.all([dataFetch, schemaFetch]).then(res => {
      // const [data, schema] = res;
      let data = res[0];
      let schema = res[1];
       const fusionDataStore = new FusionCharts.DataStore();
       const fusionTable = fusionDataStore.createDataTable(data, schema);
      this.dataSource.data = fusionTable;
    });
  }
  });
  }
  getDashboardData() {
    this.api.getDahboard().subscribe((response: Resp) => {
      if (response.statusCode != 200) return;
      this.dashboardList = response.data;
     });
  }
    gotoUser(){
      if(this.role=='SuperAdmin' || (this.role=='SubAdmin' && this.access.read==true))
          this.router.navigateByUrl('/dashboard/users');
    }
    gotoSong(){
      if(this.role=='SuperAdmin' || (this.role=='SubAdmin' && this.access.read==true))
          this.router.navigateByUrl('/dashboard/song');
    }
     gotoAlbum(){
      if(this.role=='SuperAdmin' || (this.role=='SubAdmin' && this.access.read==true))
          this.router.navigateByUrl('/dashboard/album');
    }
     gotoArtist(){
      if(this.role=='SuperAdmin' || (this.role=='SubAdmin' && this.access.read==true))
          this.router.navigateByUrl('/dashboard/artist');
    }
     
}
