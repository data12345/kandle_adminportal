import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../../services/api/api.service';
import { Resp } from '../../models/Resp';
import { FilterBody } from '../../requests/filter-body';
import Swal from 'sweetalert2';
import { UrlService } from 'src/app/services/url/url.service';
import { CommonService } from 'src/app/services/common/common.service';
import { artist } from 'src/app/models/artist';
import { SongModalComponent } from './song-modal/song-modal.component';
import { ViewSongModalComponent } from './view-song-modal/view-song-modal.component';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-song',
  templateUrl: './song.component.html',
  styleUrls: ['./song.component.scss']
})
export class SongComponent implements OnInit {
  totalItems: number;
  serialNumber = 0;
  filterBody = new FilterBody();
  songData: Array<any> = [];
  imageUrl: string;
  flags = {
    isEdit: false
  };
  config: any = {
    id: "page",
    currentPage: 1,
    itemsPerPage: 10
  };
  currentPage: Number;
  @ViewChild(SongModalComponent, { static: false }) public modalComponent: SongModalComponent;
  @ViewChild(SongModalComponent, { static: false }) public AddmodalComponent: SongModalComponent;
  @ViewChild(ViewSongModalComponent, { static: false }) public viewmodalComponent: ViewSongModalComponent;

  role: any;
  access: any;
  searchText: string;
  albumId: any;
  artistId: any;

  constructor(
    private api: ApiService,
    private url: UrlService,
    private common: CommonService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.currentPage = 1;
    this.imageUrl = this.url.imageUrl;
    if (JSON.stringify(localStorage.getItem('Kandle_Admin'))) {
      this.role = JSON.parse(localStorage.getItem('Kandle_Admin'))._value.role
      this.access = JSON.parse(localStorage.getItem('Kandle_Admin'))._value.access
    }
    this.activatedRoute.params.subscribe((param: any) => {
      this.artistId = param.id;
      console.log(this.artistId, "iddd")
    })
    this.getSong(this.currentPage);

  }
  getSong(pageNo) {
    let data = {
      pageNo: pageNo
    }
    if (this.artistId) {
      data['artistId'] = this.artistId;
    }
    if (this.albumId) {
      data['albumId'] = this.albumId;
    }
    if (this.filterBody.searchText) {
      data['search'] = this.filterBody.searchText.trim();
    }
    this.api.getAllSong(data).subscribe((response: Resp) => {
      if (response.statusCode != 200) return;
      this.songData = response.data.songData;
      this.totalItems = response.data.count;
      this.config = {
        id: "page",
        currentPage: pageNo,
        itemsPerPage: 10,
        totalItems: response.data.count
      };
    });
  }
  searchSong() {
    this.filterBody.skip = 0;
    this.getSong(this.currentPage);
  }
  reset() {
    this.searchText = '';
    this.filterBody.searchText = undefined;
    this.filterBody.skip = 0;
    this.getSong(this.currentPage);
  }
  deleteSong(id: string) {
    let body = {
      'songId': id,
    }
    Swal.fire({
      title: 'Are you sure?',
      text: 'Once deleted, you will not be able to recover this Song!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085D6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      allowOutsideClick: false,
    }).then((result) => {
      if (result.value) {
        this.api.deleteSong(body).subscribe((response: Resp) => {
          if (response.statusCode != 200) return;
          Swal.fire({
            title: 'Deleted!',
            text: 'Poof! Your Song has been deleted!',
            icon: 'success'
          })
          this.getSong(this.currentPage);
          // this.ngOnInit()
        })
      }
    })
  }
  pageChange(page) {
    console.log(page)
    this.filterBody.skip = page.page - 1;
    this.serialNumber = this.filterBody.skip * this.filterBody.limit;
    this.getSong(page.page);
  }
  blockSong(item) {
    let data = {
      'songId': item._id,
      'isBlocked': item.isBlocked
    }
    this.api.verifyBlockUnBlockSong(data).subscribe((response: any) => {
      if (response.statusCode != 200)
        return this.common.errorToast(response.message)
      this.common.successToast(response.message);
      this.getSong(this.currentPage);
    })
  }

}