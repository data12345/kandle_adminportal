import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ApiService } from '../../../services/api/api.service';
import { CommonService } from '../../../services/common/common.service';
import { Resp } from '../../../models/Resp';
import * as _ from 'lodash';
declare var $: any;
@Component({
  selector: 'app-song-modal',
  templateUrl: './song-modal.component.html',
  styleUrls: ['./song-modal.component.scss']
})
export class SongModalComponent implements OnInit {
  @Input() isEdit: boolean;
  @Input() imageUrl: string;
  @Output() onAddEdit = new EventEmitter();
  src: any;
  imgsrc:any
  file: File;
  Songfile: File;
  category: string;
  album: string
  play: string;
  artistType: Array<any> = [];
  artist: Array<any> = [];
  artist1: Array<any> = [];
  songId: any;
  categoryList: any = [];
  albumList: any = [];
  PlayList: any = [];
  isSong: boolean = false
  ArtistList: Array<any> = [];
  artistlist: Array<any> = [];
  artistIds: Array<any> = [];
  formData = new FormData();
  SongName: string;
  previewUrl: any;
  isfeaturedSong: boolean = false
  isKandleSponsored:boolean =false
  flags = {
    isAdded: false,
    isUpdate: false
  };
  image: any;
  artistId: any;
  
  config: {};
  config2: {};

  currentPage: number;
  totalItems: number;
  config1: {};
  dropDownSetting: any;
  singleDropDownSetting: any;
  play1: string;
  category1: string;
  album1: string;
  isSoportify: any;
  description: string;
  constructor(
    private api: ApiService,
    private common: CommonService,
  ) { }

  ngOnInit() {
    this.dropDownSetting = this.common.dropSetting;
    this.singleDropDownSetting = this.common.singleDropSetting;
   this.getcategorylist();
    // this.getArtistList();
    this.getAlbumList();
    this.getPlayList();
  }
  getcategorylist(){
    this.api.getAllCategoriesData('').subscribe((res: Resp) => {
       if (res['statusCode'] != 200) {
          // return this.common.errorToast(res.message);
        }
      this.categoryList = res.data.categoryData
      this.categoryList.map((i) => {
        i.fullName = i.name
      });
      this.config = {
        displayKey: "fullName", //if objects array passed which key to be displayed defaults to description
        search: true, //true/false for the search functionlity defaults to false,
        height: "150px", //height of the list so that if there are more no of items it can show a scroll defaults to auto. With auto height scroll will never appear
        placeholder: "Select Category", // text to be displayed when no item is selected defaults to Select,
        customComparator: () => { }, // a custom function using which user wants to sort the items. default is undefined and Array.sort() will be used in that case,
        limitTo: this.categoryList.length, // a number thats limits the no of options displayed in the UI similar to angular's limitTo pipe
        moreText: "more", // text to be displayed whenmore than one items are selected like Option 1 + 5 more
        noResultsFound: "No results found!", // text to be displayed when no items are found while searching
        searchPlaceholder: "Search", // label thats displayed in search input,
        searchOnKey: "fullName" // key on which search should be performed this will be selective search. if undefined this will be extensive search on all keys
      };
    })
  }
  getPlayList() {
    this.api.getAllPlaytListData('').subscribe((res: Resp) => {
        if (res['statusCode'] != 200) {
          // return this.common.errorToast(res.message);
        }
      this.PlayList = res.data.playlistData
      this.PlayList.map((i) => { i.fullName = i.name });
      this.config1 = {
        displayKey: "fullName", //if objects array passed which key to be displayed defaults to description
        search: true, //true/false for the search functionlity defaults to false,
        height: "150px", //height of the list so that if there are more no of items it can show a scroll defaults to auto. With auto height scroll will never appear
        placeholder: "Select Playlist", // text to be displayed when no item is selected defaults to Select,
        customComparator: () => { }, // a custom function using which user wants to sort the items. default is undefined and Array.sort() will be used in that case,
        limitTo: this.PlayList.length, // a number thats limits the no of options displayed in the UI similar to angular's limitTo pipe
        moreText: "more", // text to be displayed whenmore than one items are selected like Option 1 + 5 more
        noResultsFound: "No results found!", // text to be displayed when no items are found while searching
        searchPlaceholder: "Search", // label thats displayed in search input,
        searchOnKey: "fullName" // key on which search should be performed this will be selective search. if undefined this will be extensive search on all keys
      };
    })
  }
  getAlbumList() {
    // this.common.showSpinner()
    this.api.getAllAlbumData('').subscribe((res: Resp) => {
        if (res['statusCode'] == 200) {
       
      this.albumList = res.data.albumData
      // this.common.hideSpinner
      this.albumList.map((i) => { i.fullName = i.name });
      this.config2 = {
        displayKey: "fullName", //if objects array passed which key to be displayed defaults to description
        search: true, //true/false for the search functionlity defaults to false,
        height: "150px", //height of the list so that if there are more no of items it can show a scroll defaults to auto. With auto height scroll will never appear
        placeholder: "Select Album", // text to be displayed when no item is selected defaults to Select,
        customComparator: () => { }, // a custom function using which user wants to sort the items. default is undefined and Array.sort() will be used in that case,
        limitTo: this.albumList.length, // a number thats limits the no of options displayed in the UI similar to angular's limitTo pipe
        moreText: "more", // text to be displayed whenmore than one items are selected like Option 1 + 5 more
        noResultsFound: "No results found!", // text to be displayed when no items are found while searching
        searchPlaceholder: "Search", // label thats displayed in search input,
        searchOnKey: "fullName" // key on which search should be performed this will be selective search. if undefined this will be extensive search on all keys
      };}
    })
  }
 
  onEditSelectSong(){
    console.log(this.category,this.category1)
    this.category1 = ''
    this.category = ''
    console.log('this.data')
    document.getElementById('openArtistModal').click();
  }
  onItemSelect(item) {
    this.artist1.push(item.item_id);
    console.log(this.artist1, item)

  }
  onDeSelect(item: any) {
    let index
    index = this.artist1.indexOf(item.item_id);
    if (index > -1) this.artist1.splice(index, 1);
    console.log(this.artist)
  }
  onSelectAll(items: any) {
    console.log(items);
    items.forEach(ele => {
      this.artist1.push(ele.item_id);
      console.log(this.artist1)
    });
  }
  getArtistList(id) {
    let data={
      "albumId":id
      }
    const list1 = [];
    this.api.getAllArtistById(data).subscribe((res: Resp) => {
    if (res['statusCode'] != 200) 
        return this.common.errorToast(res.message);
       this.artistType = res.data.artistData
      this.common.hideSpinner();

      res.data.artistData.forEach(ele => {
        list1.push({
          item_id: ele.artistId._id,
          item_text: ele.artistId.name
        });
        this.ArtistList = list1;
        console.log(this.ArtistList,"artistList");
        const list = [];
     
        this.artistIds.forEach(ele => {
          const obj = this.artistType.find(o => o.artistId._id.toString() == ele.toString());
        console.log(obj,"=========")
          if (obj) list.push({item_id: obj.artistId._id, item_text: obj.artistId.name});
        });
        this.artist1 =this.artistIds;
        this.artist=list
      });
    
      });
    
     
  }
  // onImageSelect(e) {
  //   const file = e.target.files[0];
  //   this.formData.delete('image');
  //   if (file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/jpeg') {
  //     const reader = new FileReader();
  //     reader.onload = (event: any) => {
  //       this.src = event.target.result;
  //     };
  //     reader.readAsDataURL(e.target.files[0]);
  //     this.file = file;
  //     this.formData.append('image', this.file);
  //     this.api.uploadFileWithS3(this.formData).subscribe((res: any) => {
  //       if (res.statusCode == 200) {
  //         this.common.successToast(res['message'])
  //         this.image = res.data.orignal;
  //         console.log(this.image, res.data)
  //         this.formData = new FormData();
  //       } else {
  //         this.common.errorToast(res['message'])
  //       }
  //     })
  //   } else {
  //     this.error('Selected file is not image.');
  //   }
  // }
  onSongSelect(e) {
    console.log(e.target)
    const file = e.target.files[0];
    console.log(file.type, "type")
    if (file.type == 'audio/mpeg') {
      const reader = new FileReader();
      reader.onload = (event: any) => {
        this.imgsrc = event.target.result;
        console.log(this.imgsrc,"song",e.target.files[0])
      };
      reader.readAsDataURL(e.target.files[0]);
      this.Songfile = file;
      this.formData.append('image', this.Songfile);
      this.api.uploadFileWithS3(this.formData).subscribe((res: any) => {
        if (res.statusCode == 200) {
          this.common.successToast(res['message'])
          this.previewUrl = this.imageUrl + res.data.orignal;
          this.imgsrc = this.previewUrl
          this.formData = new FormData();
          console.log(this.image, res.data)
        } else {
          this.common.errorToast(res['message'])
        }
      })
    } else {
      this.error('Selected file is not song.');
    }
  }
  onImageRemove() {
    this.src = null;
    this.imgsrc =null;
    $('#artistFile').val('');
  }
  onEditSelect(data) {
    this.SongName = data.name;
    this.description = data.description
    this.isEdit = true;
    this.previewUrl = data.previewUrl,
      this.isfeaturedSong = data.isfeaturedSong,
      // this.isKandleSponsored =data.isKandleSponsored
      // this.isSong = data.isSong
      this.artistIds=data.artistIds
      this.isSoportify=data.isSoportify
    this.getAlbumList();
    this.getPlayList()
    setTimeout(() => {
    
    console.log(data, this.categoryList,"fff")
    for (let list of this.categoryList) {
      if (list._id === data['categoryId']){
      console.log(list._id,data['categoryId'])
        this.category1 = data['categoryId']
      this.category = list['name']
    }
  }
   
 
    console.log(this.artist,"artist")
    // this.src = `${data.image}`;
    this.imgsrc=`${data.previewUrl}`;
  
    // this.image = data.image;
    this.previewUrl = data.previewUrl
    // this.artistId = data.artistIds
    this.songId = data._id
    if (data.albumId) {
      for (let list of this.albumList) {
        if (list._id === data['albumId']){
          this.album = list['name']
        this.album1 = data['albumId']
        this.getArtistList(this.album1)
        }
      }
    }
    if (data.playlistId) {
      for (let list of this.PlayList) {
        if (list._id === data['playlistId']){
          this.play = list['name']
        this.play1 = data['playlistId']
        }
      }
    }
    }, 3000);
    
    this.formData.append('id', data._id);
    document.getElementById('openArtistModal').click();
  }
  AddSong() {
    console.log(this.category1,this.category,"------",this.play,this.play1)
    // if (!this.file) return this.error('Please select image first.');
    if (!this.Songfile) return this.error('Please upload song first.');
    if (!this.category1) return this.error('Please Select Category.');
    if (!this.artist1) return this.error('Please Select Artist.');
    if (!this.album1) return this.error('Please Select Album.');


    this.flags.isAdded = true;
    let body = {
      "name": this.SongName,
      "image": this.image,
      "previewUrl": this.previewUrl,
      "categoryId": this.category1,
      // "playlistId": this.play1,
      // "albumId": this.album1,
      "artistIds": this.artist1,
      "openUrl": this.previewUrl,
      "isfeaturedSong": this.isfeaturedSong,
      // "isKandleSponsored":this.isKandleSponsored,
      // "isSong": this.isSong,
      "description" : this.description

    }
    if (this.play1) {
      body['playlistId'] = this.play1;
    }
    if (this.album1) {
      body['albumId'] = this.album1;
    }
    console.log(body, "body")
    this.api.addSong(body).subscribe((response: Resp) => {
      this.flags.isAdded = false;
      if (response['statusCode'] != 200) {
        return this.common.errorToast(response.message);
      }
      this.onCancel();
      this.common.successToast('Song added successfully!');
      this.onAddEdit.emit(true);
    }, error => {
      this.flags.isAdded = false;
    });
  }
  editSong() {
    if (!this.category1 && this.isSoportify == false) return this.error('Please Select Category.');
    if (!this.album1 && this.isSoportify == false) return this.error('Please Select Album.');
    if (!this.artist1 && this.isSoportify == false) return this.error('Please Select Artist.');

    this.flags.isUpdate = true;
    let body = {
      "name": this.SongName,
      "image": this.image,
      "previewUrl": this.previewUrl,
      // "categoryId": this.category1,
      // "playlistId": this.play1,
      // "albumId": this.album1,
      // "artistIds": this.artist1,
      "songId": this.songId,
      "openUrl": this.previewUrl,
      "isfeaturedSong": this.isfeaturedSong,
      // "isKandleSponsored":this.isKandleSponsored,
      // "isSong": this.isSong,
      // "description" : this.description

    }
    if (this.artist1) {
      body['artistIds'] = this.artist1;
    }
    if (this.category1) {
      body['categoryId'] = this.category1;
    }
    if (this.description) {
      body['description'] = this.description;
    }
    if (this.play1) {
      body['playlistId'] = this.play1;
    }
    if (this.album1) {
      body['albumId'] = this.album1;
    }
    this.api.updateSong(body).subscribe((response: Resp) => {
      this.flags.isUpdate = false;
      if (response.statusCode != 200) return this.common.errorToast(response.message);
      this.onCancel();
      this.common.successToast('Song updated successfully!');
      this.onAddEdit.emit(true);
    }, error => {
      this.flags.isUpdate = false;
    });
  }
  onChangePlayListById(event) {
    if(event.value){
    this.play1 = event.value._id
    console.log(event, "event")
  }
}
  onChangeCategoryById(event) {
    console.log(event.value)
    if(event.value){
    this.category1 = event.value._id
    console.log(event, "event")
  }
  }
  onChangeAlbumById(event) {
    if(event.value){
    this.album1 = event.value._id
    if(this.album1){
    this.getArtistList(this.album1)
    console.log(event, "event")
  }
}
  }
  onCancel() {
    this.SongName = '';
    this.previewUrl = '';
    this.play=''
    this.PlayList =[];
    this.albumList = [];
    this.categoryList =[];
    this.play=undefined
    this.play1 = '';
    this.play1=undefined
   this.description=''

    // this.category = '';
    this.category = undefined;
    this.album1 = '';
    this.album1=undefined;
    this.category1=undefined;
    this.category1 = '';
    this.album = '';
    this.album=undefined
    this.artist1 = []
    this.artist = []
    this.artistlist=[]
    this.artistType=[]
    this.isfeaturedSong = false;
    this.isKandleSponsored =false
    this.isSong = false
    this.file = null;
    this.src = null;
    this.Songfile = null;
    this.imgsrc = null;
    console.log(this.category,this.category1,"category======")
    $('#artistFile').val('');
    this.formData = new FormData();
    this.isEdit = false;
    this.getAlbumList();
    this.getPlayList();
    this.getcategorylist();
    document.getElementById('closeArtistModal').click();
  }
  error = message => {
    this.common.errorToast(message);
  }
  // isfeaturedSongs(item) {
  //   console.log(item.isfeaturedSong)
  //   this.isfeaturedSong = item.isfeaturedSong
  // }
  // isSongItem(item) {
  //   console.log(item.isSong)
  //   this.isSong = item.isSong
  // }
  // isKandleSponsoreds(item) {
  //   console.log(item.isKandleSponsored)
  //   this.isKandleSponsored = item.isKandleSponsored
  // }

}


