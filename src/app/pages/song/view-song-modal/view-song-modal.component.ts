import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ApiService } from '../../../services/api/api.service';
import { CommonService } from '../../../services/common/common.service';
import { Resp } from '../../../models/Resp';
import { UrlService } from 'src/app/services/url/url.service';
declare var $: any;
@Component({
  selector: 'app-view-song-modal',
  templateUrl: './view-song-modal.component.html',
  styleUrls: ['./view-song-modal.component.scss']
})
export class ViewSongModalComponent implements OnInit {
  @Input() isEdit: boolean;
  @Input() imageUrl: string;
  @Output() onAddEdit = new EventEmitter();
  src: any;
  file: File;
  formData = new FormData();
  artistName: string;
  flags = {
    isAdded: false,
    isUpdate: false
  };
  image: any;
  artistId: any;
  id: any;
  songData: any;
  previewUrl: any;
  constructor(
    private api: ApiService,
    private common: CommonService,
    private url:UrlService
  ) { }

  ngOnInit() {
    this.imageUrl=this.url.imageUrl

  }
  onCancel() {
    // this.artistName = '';
    // this.file = null;
    // this.src = null;
    $('#artistFile').val('');
    document.getElementById('closeSongModal').click();
  }
  onEditSelect1(data){
    console.log(data,"datatat")
    document.getElementById('openSongModal').click();
    this.id=data._id
    this.getSong()
  }
  getSong() {
    let data={
    songId:this.id}
    
    this.api.getSongDetail(data).subscribe((response: Resp) => {
      if (response.statusCode != 200) return;
      this.songData = response.data.songData;
      this.previewUrl=this.imageUrl+response.data.songData.previewUrl
      this.src = `${this.songData.image}`;
      // this.image= this.songData.image;
      });
  }
}
