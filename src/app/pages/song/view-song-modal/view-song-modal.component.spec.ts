import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewSongModalComponent } from './view-song-modal.component';

describe('ViewSongModalComponent', () => {
  let component: ViewSongModalComponent;
  let fixture: ComponentFixture<ViewSongModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewSongModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewSongModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
