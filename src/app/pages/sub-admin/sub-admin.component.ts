import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../services/api/api.service';
import {CommonService} from '../../services/common/common.service';
import {FilterBody} from '../../requests/filter-body';
import {Resp} from '../../models/Resp';
import {SubAdminList} from '../../models/sub-admin-list';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-sub-admin',
  templateUrl: './sub-admin.component.html',
  styleUrls: ['./sub-admin.component.scss']
})
export class SubAdminComponent implements OnInit {
  totalItems: number;
  currentPage = 1;
  serialNumber = 0;
  filterBody = new FilterBody();
  subAdminList: Array<SubAdminList> = [];
  config: any = {
    id: "page",
    currentPage: 1,
    itemsPerPage: 10
  };
  searchText: string;
  constructor(
    private api: ApiService,
    private common: CommonService
  ) { }

  ngOnInit() {
    this.filterBody.searchText='';
    this.currentPage = 1;

    this.getAllSubAdmin(this.currentPage);
  }
  getAllSubAdmin(pageNo) {
    let data={
      pageNo:pageNo}
      if (this.filterBody.searchText) {
        data['search'] = this.filterBody.searchText.trim();
    }
    this.api.getAllSubAdmin(data).subscribe((response: Resp) => {
      if (response.statusCode != 200) return;
      this.subAdminList = response.data.subAdminData;
      this.totalItems = response.data.count;  
      console.log(this.subAdminList,"adata")
      this.config = {
        id: "page",
        currentPage: pageNo,
        itemsPerPage: 10,
        totalItems:response.data.count
      };

    });
    //   this.subAdminList = response.data;
    //   this.totalItems = response.count;
    // });
  }
  deleteAdmin(id: string) {
    let body ={
      'adminId':id,
    }
    Swal.fire({
      title: 'Are you sure?',
      text: 'Once deleted, you will not be able to recover this sub-admin!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085D6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      allowOutsideClick: false,
    }).then((result) => {
      if (result.value) {
        this.api.deleteSubAdmin(body).subscribe((response: Resp) => {
          if (response.statusCode !=200) return;
          Swal.fire({
            title: 'Deleted!',
            text: 'Poof! Your sub-admin has been deleted!',
            icon: 'success'
          })
          this.getAllSubAdmin(this.currentPage);
          // this.ngOnInit()
        })
      }
    })
  }

  updateAccess(item: SubAdminList) {
    item.id = item._id;
    let body = {
      // 'address': this.body.address,
      'adminId': item._id,
      // 'city': this.body.city,
      // 'countryCode': this.body.countryCode,
      // 'email': this.body.email,
      // 'firstName': this.body.firstName,
      // 'lastName': this.body.lastName,
      // 'phoneNo': this.body.phoneNo,
      // 'role': this.body.role,
      // 'state': this.body.state,
      // 'zipCode': this.body.zipCode,
      'access': item.access
    }
    this.api.editSubAdmin(body).subscribe((response: Resp) => {
      if (response.statusCode != 200)
      return this.common.errorToast(response.message);
      this.common.successToast('Sub admin updated successfully!');
    });
  }
  
  onPageChange(page) {
    this.filterBody.skip = page.page - 1;
    this.serialNumber = this.filterBody.skip * this.filterBody.limit;
    this.getAllSubAdmin(page.page);
  }
  searchUser(){
    // this.filterBody.searchText=this.searchText.trim();
    this.filterBody.skip=0;
    this.getAllSubAdmin(this.currentPage);
  }

  reset(){
    this.searchText='';
    this.filterBody.searchText=undefined;
    this.filterBody.skip=0;
    this.getAllSubAdmin(this.currentPage);
  }
}
