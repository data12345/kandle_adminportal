import { Component, OnInit, ViewChild } from '@angular/core';
import { Access, AddSubAdminBody } from "../../../requests/add-sub-admin-body";
import { ApiService } from "../../../services/api/api.service";
import { CommonService } from "../../../services/common/common.service";
import { ActivatedRoute, Router } from "@angular/router";
import { Resp } from "../../../models/Resp";
import countryCode from 'src/app/requests/countrycode';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';

@Component({
  selector: 'app-edit-sub-admin',
  templateUrl: './edit-sub-admin.component.html',
  styleUrls: ['./edit-sub-admin.component.scss']
})
export class EditSubAdminComponent implements OnInit {
  history = window.history;
  access = new Access();
  adminId: string;
  body = new AddSubAdminBody();
  flags = {
    isUpdate: false
  };
  countryCode: any;
  codes = [];
  constructor(
    private api: ApiService,
    private common: CommonService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }
  @ViewChild("placesRef", { static: false }) placesRef: GooglePlaceDirective;

  ngOnInit() {
    this.codes = countryCode;
    this.activatedRoute.params.subscribe((param: any) => {
      this.adminId = param.id;
      this.getAdminDetail();
    });
  }
  getAdminDetail() {
    this.api.getAdminDetails({ "adminId": this.adminId }).subscribe((response: Resp) => {
      if (response.statusCode != 200) return;
      this.body = response.data.subAdminData[0];
      console.log(this.body)
      this.access = this.body.access;
    });
  }
  editSubAdmin() {
    this.flags.isUpdate = true;
    let body = {
      'address': this.body.address,
      'adminId': this.adminId,
      'city': this.body.city,
      'countryCode': this.body.countryCode,
      'email': this.body.email,
      'firstName': this.body.firstName,
      'lastName': this.body.lastName,
      'phoneNo': this.body.phoneNo,
      'role': this.body.role,
      'state': this.body.state,
      'zipCode': this.body.zipCode,
      'access': this.access
    }
    this.api.editSubAdmin(body).subscribe((response: Resp) => {
      this.flags.isUpdate = false;
      if (response.statusCode != 200)
        return this.common.errorToast(response.message);
      this.common.successToast('Sub admin updated successfully!');
      this.router.navigateByUrl('/dashboard/sub-admin');
    }, error => {
      this.flags.isUpdate = false;
    });
  }
  public handleAddressChange(addressValue) {
    if (addressValue.formatted_address) {
      this.body.address = addressValue.formatted_address
      console.log(addressValue, "addesss")
    }
    addressValue.address_components.forEach(key => {
      if (key.types[0] == 'locality') {
        this.body.city = key.long_name
      }
      if (key.types[0] === 'administrative_area_level_1') {
        this.body.state = key.long_name
      }
      // if (key.types[0] === 'country') {
      //   this.body.Co = key.long_name
      // }
      if (key.types[0] === 'postal_code') {
        this.body.zipCode = key.long_name
      }
      // this.body.latitude = addressValue.geometry.location.lat()
      // this.body.longitude = addressValue.geometry.location.lng()
    })

  }
}
