import { Component, OnInit, ViewChild } from '@angular/core';
import {Access, AddSubAdminBody} from "../../../requests/add-sub-admin-body";
import {ApiService} from "../../../services/api/api.service";
import {CommonService} from "../../../services/common/common.service";
import {Router} from "@angular/router";
import {Resp} from "../../../models/Resp";
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import countryCode from 'src/app/requests/countrycode';

@Component({
  selector: 'app-add-sub-admin',
  templateUrl: './add-sub-admin.component.html',
  styleUrls: ['./add-sub-admin.component.scss']
})
export class AddSubAdminComponent implements OnInit {
  history = window.history;
  access = new Access();
  body = new AddSubAdminBody();
  flags = {
    isAdded: false
  };
  countryCode:any;
  codes=[];

  constructor(
    private api: ApiService,
    private common: CommonService,
    private router: Router
  ) { }
  @ViewChild("placesRef", { static: false }) placesRef: GooglePlaceDirective;

  ngOnInit() {
    this.codes = countryCode;

  }
  addSubAdmin() {
    this.flags.isAdded = true;
    this.body.access = this.access;
    this.api.addSubAdmin(this.body).subscribe((response: Resp) => {
      this.flags.isAdded = false;
      if (response.statusCode != 200 )
{   return this.common.errorToast(response.message);
        
    }
           this.common.successToast('Sub admin added successfully!');
            this.router.navigateByUrl('/dashboard/sub-admin');
    }, error => {
      this.flags.isAdded = false;
    });
  }
  public handleAddressChange(addressValue) {
    if (addressValue.formatted_address) {
      this.body.address = addressValue.formatted_address
      console.log( addressValue, "addesss")
    }
    addressValue.address_components.forEach(key => {
      if (key.types[0] == 'locality') {
        this.body.city = key.long_name
      }
      if (key.types[0] === 'administrative_area_level_1') {
        this.body.state = key.long_name
      }
      // if (key.types[0] === 'country') {
      //   this.body.Co = key.long_name
      // }
      if (key.types[0] === 'postal_code') {
        this.body.zipCode = key.long_name
      }
      // this.body.latitude = addressValue.geometry.location.lat()
      // this.body.longitude = addressValue.geometry.location.lng()
       })
    
  }
}
