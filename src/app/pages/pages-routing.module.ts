import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {UsersComponent} from './users/users.component';
import {CategoryComponent} from './category/category.component';
import {PagesComponent} from './pages.component';
import {AddUserComponent} from './users/add-user/add-user.component';
import {EditUserComponent} from './users/edit-user/edit-user.component';
import {SettingComponent} from './setting/setting.component';
import {SubAdminComponent} from './sub-admin/sub-admin.component';
import {AddSubAdminComponent} from './sub-admin/add-sub-admin/add-sub-admin.component';
import {EditSubAdminComponent} from './sub-admin/edit-sub-admin/edit-sub-admin.component';
import {AdminProfileComponent} from './admin-profile/admin-profile.component';
import { DocumentModalComponent } from './users/document-modal/document-modal.component';
import { CmsPagesComponent } from './cms-pages/cms-pages.component';
import { VersionComponent } from './version/version.component'; // <-- import the module
import { ArtistComponent } from './artist/artist.component'; // <-- import the module
import { AlbumComponent } from './album/album.component'; // <-- import the module
import { SongComponent } from './song/song.component';
import { PlaylistComponent } from './playlist/playlist.component'; // <-- import the module
import { ArtistSongComponent } from './artist/artist-song/artist-song.component';
import { CelebrityComponent } from './celebrity/celebrity.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { ReportComponent } from './report/report.component';
import { SubscriptionComponent } from './subscription/subscription.component';


const routes: Routes = [
  {path: '', component: PagesComponent, children: [
      {path: '', redirectTo: '/home', pathMatch: 'full'},
      {path: 'home', component: DashboardComponent},
      {path: 'users', component: UsersComponent},
      {path: 'categories', component: CategoryComponent},
      {path: 'add-user', component: AddUserComponent},
      {path: 'edit-user/:id', component: EditUserComponent},
      {path: 'document/:id', component: DocumentModalComponent},
      {path: 'setting', component: SettingComponent},
      {path: 'sub-admin', component: SubAdminComponent},
      {path: 'add-sub-admin', component: AddSubAdminComponent},
      {path: 'edit-sub-admin/:id', component: EditSubAdminComponent},
      {path: 'profile', component: AdminProfileComponent},
      {path: 'cms', component:CmsPagesComponent},
      {path: 'artist', component:ArtistComponent},
      {path:'album', component : AlbumComponent},
      {path:'song', component : SongComponent},
      {path: 'artist-song/:id', component: ArtistSongComponent},
      {path: 'report', component: ReportComponent},
      {path:'subscription',component:SubscriptionComponent},

      {path:'playlist', component : PlaylistComponent},


      {path:'app-version', component : VersionComponent},
      {path:'contact', component : ContactUsComponent},   
        {path:'celebrity', component : CelebrityComponent},

      {path: '**', loadChildren: () => import('../page-not-found/page-not-found.module').then(m => m.PageNotFoundModule)},

    ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
