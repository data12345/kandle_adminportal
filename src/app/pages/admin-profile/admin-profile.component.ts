import { Component, OnInit } from '@angular/core';
import {ApiService} from "../../services/api/api.service";
import {CommonService} from "../../services/common/common.service";
import {Admin} from "../../models/Admin";
import {Resp} from "../../models/Resp";
import {ChangePasswordBody} from "../../requests/change-password-body";
import {LocalStorageService} from "angular-web-storage";

@Component({
  selector: 'app-admin-profile',
  templateUrl: './admin-profile.component.html',
  styleUrls: ['./admin-profile.component.scss']
})
export class AdminProfileComponent implements OnInit {
  history = window.history;
  admin = new Admin();
  changePassBody = new ChangePasswordBody();
  flags = {
    isUpdate: false,
    isPassChange: false
  };

  constructor(
    private api: ApiService,
    private localStorage: LocalStorageService,
    private common: CommonService
  ) { }

  ngOnInit() {
    this.getProfile();
  }
  getProfile() {
    this.api.getProfile('').subscribe((response: Resp) => {
      if (response.statusCode!= 200) return;
      this.admin = response.data;
      this.localStorage.set('admin_name', response.data.firstName );
      this.common.username = response.data.firstName  ;
      this.common.setgetAdminName();
    });
  }
  updateProfile() {
    let body ={
      'firstName':this.admin.firstName,
      'email':this.admin.email,
      'phoneNo':this.admin.phoneNo

    }
    this.flags.isUpdate = true;
    this.api.updateProfile(body).subscribe((response: Resp) => {
      this.flags.isUpdate = false;
      if (response.statusCode != 200) return;
      this.common.successToast('Profile updated successfully')
      this.getProfile();
    }, error => {
      this.flags.isUpdate = false;
    });
  }
  changePassword() {
    if (this.changePassBody.confirmPassword != this.changePassBody.newPassword) 
    return this.common.errorToast('New password & confirm password does not match.');
    this.flags.isPassChange = true;
    this.api.updateProfile({password :this.changePassBody.confirmPassword,oldPassword:this.changePassBody.oldPassword}).subscribe((response: Resp) => {
      this.flags.isPassChange = false;
      if (response.statusCode != 200)
       return this.common.errorToast(response.message);
      this.common.successToast('Password changed successfully!');
      this.changePassBody = new ChangePasswordBody();
    }, error => {
      this.flags.isPassChange = false;
    });
  }

}
