import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ApiService } from '../../../services/api/api.service';
import { CommonService } from '../../../services/common/common.service';
import { Resp } from '../../../models/Resp';
declare var $: any;
@Component({
  selector: 'app-playlist-modal',
  templateUrl: './playlist-modal.component.html',
  styleUrls: ['./playlist-modal.component.scss']
})
export class PlaylistModalComponent implements OnInit {
  @Input() isEdit: boolean;
  @Input() imageUrl: string;
  @Output() onAddEdit = new EventEmitter();
  src: any;
  file: File;
  formData = new FormData();
  categoryName: string;
  flags = {
    isAdded: false,
    isUpdate: false
  };
  config: {};
  dropDownSetting: any;
  singleDropDownSetting: any;
  categoryList: any = [];
  image: any;
  playlistId: any;
  category1: any;
  category: any='';
  constructor(
    private api: ApiService,
    private common: CommonService,
  ) { }

  ngOnInit() {
    this.dropDownSetting = this.common.dropSetting;
    this.singleDropDownSetting = this.common.singleDropSetting;
   this.getcategoryList()
  }
getcategoryList(){
  this.api.getAllCategoriesData('').subscribe((res: any) => {
    this.categoryList = res.data.categoryData
    this.categoryList.map((i) => {
      console.log(i)
      i.fullName = i.name
    });
    this.config = {
      displayKey: "fullName", //if objects array passed which key to be displayed defaults to description
      search: true, //true/false for the search functionlity defaults to false,
      height: "150px", //height of the list so that if there are more no of items it can show a scroll defaults to auto. With auto height scroll will never appear
      placeholder: "Select Category", // text to be displayed when no item is selected defaults to Select,
      customComparator: () => { }, // a custom function using which user wants to sort the items. default is undefined and Array.sort() will be used in that case,
      limitTo: this.categoryList.length, // a number thats limits the no of options displayed in the UI similar to angular's limitTo pipe
      moreText: "more", // text to be displayed whenmore than one items are selected like Option 1 + 5 more
      noResultsFound: "No results found!", // text to be displayed when no items are found while searching
      searchPlaceholder: "Search", // label thats displayed in search input,
      searchOnKey: "fullName" // key on which search should be performed this will be selective search. if undefined this will be extensive search on all keys
    };
  })
}
  onImageSelect(e) {
    const file = e.target.files[0];
    this.formData.delete('image');
    if (file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/jpeg') {
      const reader = new FileReader();
      reader.onload = (event: any) => {
        this.src = event.target.result;
      };
      reader.readAsDataURL(e.target.files[0]);
      this.file = file;
      this.formData.append('image', this.file);
      this.api.uploadFileWithS3(this.formData).subscribe((res: any) => {
        if (res.statusCode == 200) {
          this.common.successToast(res['message'])
          this.image =  this.imageUrl + res.data.orignal;
          console.log(this.image,res.data)
        } else {
          this.common.errorToast(res['message'])
        }
      })
    } else {
      this.error('Selected file is not image.');
    }
    
  }
  onChangeCategoryById(event) {
    if(event.value){
    this.category1 = event.value._id
    console.log(event, "event")
  }
  }
  onImageRemove() {
    this.src = null;
    $('#categoryFile').val('');
  }
  onEditSelect(data) {
    this.categoryName = data.name;
    this.isEdit = true;
    this.src = `${data.image}`;
    this.image= data.image;
    this.playlistId=  data._id
    for (let list of this.categoryList) {
      if (list._id === data['categoryId']){
        this.category1 = data['categoryId']
      this.category = list['name']
    }
  }
    document.getElementById('openCategoryModal').click();
  }
  addCategory() {
    console.log(this.category,this.category1,"===================")
    if (!this.file) return this.error('Please select image first.');
    this.flags.isAdded = true;
    let body = {
      "name": this.categoryName,
      "image": this.image,
      "categoryId":this.category1
    }
    this.api.addPlaylist(body).subscribe((response: Resp) => {
      this.flags.isAdded = false;
      if (response['statusCode'] != 200) {
        return this.common.errorToast(response.message);}
      this.onCancel();
      this.common.successToast('Playlist added successfully!');
      this.onAddEdit.emit(true);
    }, error => {
      this.flags.isAdded = false;
    });
  }
  editCategory() {
    this.flags.isUpdate = true;
    let body = {
      "name": this.categoryName,
      "image": this.image,
      "categoryId":this.category1,
      "playlistId":this.playlistId
   
    }
    this.api.updatePlaylist(body).subscribe((response: Resp) => {
      this.flags.isUpdate = false;
      if (response.statusCode != 200) return this.common.errorToast(response.message);
      this.onCancel();
      this.common.successToast('Playlist updated successfully!');
      this.onAddEdit.emit(true);
    }, error => {
      this.flags.isUpdate = false;
    });
  }
  onCancel() {
    this.categoryName = '';
    this.category = null;
    this.category1 = '';
    this.category = '';
    this.categoryList = []
    this.file = null;
    this.src = null;
    $('#categoryFile').val('');
    this.formData = new FormData();
    this.getcategoryList();
    this.isEdit = false;
    document.getElementById('closeCategoryModal').click();
  }
  error = message => {
    this.common.errorToast(message);
  }

}
