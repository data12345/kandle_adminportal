import {Component, OnInit, ViewChild} from '@angular/core';
import {ApiService} from '../../services/api/api.service';
import {Resp} from '../../models/Resp';
import {FilterBody} from '../../requests/filter-body';
import {Categories} from '../../models/categories';
import {UrlService} from '../../services/url/url.service';
import {PlaylistModalComponent} from "./playlist-modal/playlist-modal.component";
import Swal from 'sweetalert2';
import { CommonService } from 'src/app/services/common/common.service';
@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.scss']
})
export class PlaylistComponent implements OnInit {
  totalItems: number;
  serialNumber = 0;
  filterBody = new FilterBody();
  playlistList: Array<any> = [];
  imageUrl: string;
  flags = {
    isEdit: false
  };
  config: any = {
    id: "page",
    currentPage: 1,
    itemsPerPage: 10
  };
  currentPage: Number;
  @ViewChild(PlaylistModalComponent, {static: false}) public modalComponent: PlaylistModalComponent;
  role: any;
  access: any;
  searchText: string;

  constructor(
    private api: ApiService,
    private url: UrlService,
    private common: CommonService,
  ) { }

  ngOnInit() {
    this.currentPage = 1;
    this.imageUrl = this.url.imageUrl;
    this.getPlaylist(this.currentPage);
    if (JSON.stringify(localStorage.getItem('Kandle_Admin'))) {
      this.role = JSON.parse(localStorage.getItem('Kandle_Admin'))._value.role
      this.access = JSON.parse(localStorage.getItem('Kandle_Admin'))._value.access
    }
  }
  searchPlaylist(){
    this.filterBody.skip=0;
    this.getPlaylist(this.currentPage);
  }

  reset(){
    this.searchText='';
    this.filterBody.searchText=undefined;
    this.filterBody.skip=0;
    this.getPlaylist(this.currentPage);
  }
  getPlaylist(pageNo) {
    let data={
      pageNo:pageNo}
      if (this.filterBody.searchText) {
        data['search'] = this.filterBody.searchText.trim();
    }
    this.api.getAllplaylist(data).subscribe((response: Resp) => {
      if (response.statusCode !=200) return;
      this.playlistList = response.data.playlistData;
      this.totalItems = response.data.count;
      this.config = {
        id: "page",
        currentPage: pageNo,
        itemsPerPage: 10,
        totalItems:response.data.count
      };
    });
  }
  deletePlaylist(id: string) {
    let body ={
      'playlistId':id,
    }
    Swal.fire({
      title: 'Are you sure?',
      text: 'Once deleted, you will not be able to recover this Playlist!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085D6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      allowOutsideClick: false,
    }).then((result) => {
      if (result.value) {
        this.api.deletePlaylist(body).subscribe((response: Resp) => {
          if (response.statusCode !=200) return;
          Swal.fire({
            title: 'Deleted!',
            text: 'Poof! Your Playlist has been deleted!',
            icon: 'success'
          })
          this.getPlaylist(this.currentPage);
          // this.ngOnInit()
        })
      }
    })
  }
  pageChange(page) {
    console.log(page)
    this.filterBody.skip = page.page - 1;
    this.serialNumber = this.filterBody.skip * this.filterBody.limit;
    this.getPlaylist(page.page);
  }
  blockPlaylist(item){
    let data={
      'playlistId': item._id,
      'isBlocked':item.isBlocked
      }
      this.api.verifyBlockUnBlockPlaylist(data).subscribe((response:any)=>{
        if(response.statusCode !=200) 
       return this.common.errorToast(response.message)
        this.common.successToast(response.message);
        this.getPlaylist(this.currentPage);
      })
  }

}

