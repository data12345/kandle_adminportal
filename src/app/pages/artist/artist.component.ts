import { Component, OnInit, ViewChild } from '@angular/core';
import {ApiService} from '../../services/api/api.service';
import {Resp} from '../../models/Resp';
import {FilterBody} from '../../requests/filter-body';
import Swal from 'sweetalert2';
import { UrlService } from 'src/app/services/url/url.service';
import { CommonService } from 'src/app/services/common/common.service';
import { artist } from 'src/app/models/artist';
import { ArtistModalComponent } from './artist-modal/artist-modal.component';
@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.scss']
})
export class ArtistComponent implements OnInit {
  totalItems: number;
  serialNumber = 0;
  filterBody = new FilterBody();
  artistData: Array<artist> = [];
  imageUrl: string;
  flags = {
    isEdit: false
  };
  config: any = {
    id: "page",
    currentPage: 1,
    itemsPerPage: 10
  };
  currentPage: Number;
  @ViewChild(ArtistModalComponent, {static: false}) public modalComponent: ArtistModalComponent;
  role: any;
  access: any;
  searchText: string;

  constructor(
    private api: ApiService,
    private url: UrlService,
    private common: CommonService,
  ) { }

  ngOnInit() {
    this.currentPage = 1;
    this.imageUrl = this.url.imageUrl;
    this.getArtist(this.currentPage);
    if (JSON.stringify(localStorage.getItem('Kandle_Admin'))) {
      this.role = JSON.parse(localStorage.getItem('Kandle_Admin'))._value.role
      this.access = JSON.parse(localStorage.getItem('Kandle_Admin'))._value.access
    }
  }
  getArtist(pageNo) {
    let data={
      pageNo:pageNo}
      if (this.filterBody.searchText) {
        data['search'] = this.filterBody.searchText.trim();
    }
    this.api.getAllArtist(data).subscribe((response: Resp) => {
      if (response.statusCode != 200) return;
      this.artistData = response.data.artistData;
      this.totalItems = response.data.count;
      this.config = {
        id: "page",
        currentPage: pageNo,
        itemsPerPage: 10,
        totalItems:response.data.count
      };
    });
  }
  searchArtist(){
    this.filterBody.skip=0;
    this.getArtist(this.currentPage);
  }
reset(){
    this.searchText='';
    this.filterBody.searchText=undefined;
    this.filterBody.skip=0;
    this.getArtist(this.currentPage);
  }  
  deleteArtist(id: string) {
    let body ={
      'artistId':id,
    }
    Swal.fire({
      title: 'Are you sure?',
      text: 'Once deleted, you will not be able to recover this Artist!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085D6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      allowOutsideClick: false,
    }).then((result) => {
      if (result.value) {
        this.api.deleteArtist(body).subscribe((response: Resp) => {
          if (response.statusCode !=200) return;
          Swal.fire({
            title: 'Deleted!',
            text: 'Poof! Your Artist has been deleted!',
            icon: 'success'
          })
          this.getArtist(this.currentPage);
          // this.ngOnInit()
        })
      }
    })
  }
  pageChange(page) {
    console.log(page)
    this.filterBody.skip = page.page - 1;
    this.serialNumber = this.filterBody.skip * this.filterBody.limit;
    this.getArtist(page.page);
  }
  blockArtist(item){
    let data={
      'artistId': item._id,
      'isBlocked':item.isBlocked
      }
      this.api.verifyBlockUnBlockArtist(data).subscribe((response:any)=>{
        if(response.statusCode != 200) 
        return this.common.errorToast(response.message)
        this.common.successToast(response.message);
        this.getArtist(this.currentPage);
      })
  }
  featuredArtist(item){
    let data = {
      'artistId': item._id,
      'isfeaturedArtist': item.isfeaturedArtist
    }
    this.api.verifyBlockUnBlockArtist(data).subscribe((response:any)=>{
      if(response.statusCode != 200) 
      return this.common.errorToast(response.message)
      this.common.successToast(response.message);
      this.getArtist(this.currentPage);
    })
}

}