import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewArtistSongComponent } from './view-artist-song.component';

describe('ViewArtistSongComponent', () => {
  let component: ViewArtistSongComponent;
  let fixture: ComponentFixture<ViewArtistSongComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewArtistSongComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewArtistSongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
