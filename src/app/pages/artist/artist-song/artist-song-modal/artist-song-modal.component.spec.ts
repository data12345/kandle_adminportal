import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtistSongModalComponent } from './artist-song-modal.component';

describe('ArtistSongModalComponent', () => {
  let component: ArtistSongModalComponent;
  let fixture: ComponentFixture<ArtistSongModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtistSongModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistSongModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
