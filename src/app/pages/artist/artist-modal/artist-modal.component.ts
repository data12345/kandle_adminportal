import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ApiService } from '../../../services/api/api.service';
import { CommonService } from '../../../services/common/common.service';
import { Resp } from '../../../models/Resp';
declare var $: any;
@Component({
  selector: 'app-artist-modal',
  templateUrl: './artist-modal.component.html',
  styleUrls: ['./artist-modal.component.scss']
})
export class ArtistModalComponent implements OnInit {
  @Input() isEdit: boolean;
  @Input() imageUrl: string;
  @Output() onAddEdit = new EventEmitter();
  src: any;
  file: File;
 
  isfeaturedArtist:boolean=false;
  formData = new FormData();
  artistName: string;
  flags = {
    isAdded: false,
    isUpdate: false
  };
  image: any;
  artistId: any;
  constructor(
    private api: ApiService,
    private common: CommonService,
  ) { }

  ngOnInit() {
  }
  onImageSelect(e) {
    const file = e.target.files[0];
    this.formData.delete('image');
    if (file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/jpeg') {
      const reader = new FileReader();
      reader.onload = (event: any) => {
        this.src = event.target.result;
      };
      reader.readAsDataURL(e.target.files[0]);
      this.file = file;
      this.formData.append('image', this.file);
      this.api.uploadFileWithS3(this.formData).subscribe((res: any) => {
        if (res.statusCode == 200) {
          this.common.successToast(res['message'])
          this.image =  this.imageUrl + res.data.orignal;
          console.log(this.image,res.data)
        } else {
          this.common.errorToast(res['message'])
        }
      })
    } else {
      this.error('Selected file is not image.');
    }
    //   const file = e.target.files[0];
    //   if (e.target.files && e.target.files[0]) {
    //     var reader = new FileReader();
    //     reader.readAsDataURL(e.target.files[0]);
    //     reader.onload = (event: any) => {
    //       this.src = event.target.result;
    // };
    //     let formData = new FormData();
    //     formData.append("image", file);
    //     this.api.uploadFileWithS3(formData).subscribe((res:Resp)=>{
    //      if(res['statusCode']==200){
    //       this.common.successToast(res['message'])
    //       // this.adminForm.controls['image'].setValue(res['data']['original']);
    //       }else{
    //        this.common.errorToast(res['message'])
    //       }
    //     })
    //   }
  }
  onImageRemove() {
    this.src = null;
    $('#artistFile').val('');
  }
  onEditSelect(data) {
    this.artistName = data.name;
    this.isEdit = true;
    this.src = `${data.image}`;
    this.image= data.image;
    this.artistId=  data._id
    this.isfeaturedArtist=data.isfeaturedArtist
    this.formData.append('id', data._id);
    document.getElementById('openArtistModal').click();
  }
  AddArtist() {
    if (!this.file) return this.error('Please select image first.');
    this.flags.isAdded = true;
    let body = {
      "name": this.artistName,
      "image": this.image,
      "isfeaturedArtist":this.isfeaturedArtist,
    }
    this.api.addArtist(body).subscribe((response: Resp) => {
      this.flags.isAdded = false;
      if (response['statusCode'] != 200) {
        return this.common.errorToast(response.message);}
      this.onCancel();
      this.common.successToast('Artist added successfully!');
      this.onAddEdit.emit(true);
    }, error => {
      this.flags.isAdded = false;
    });
  }
  editArtist() {
    this.flags.isUpdate = true;
    let body = {
      "name": this.artistName,
      "image": this.image,
      "artistId":this.artistId,
      "isfeaturedArtist":this.isfeaturedArtist,
     
    }
    this.api.updateArtist(body).subscribe((response: Resp) => {
      this.flags.isUpdate = false;
      if (response.statusCode != 200) return this.common.errorToast(response.message);
      this.onCancel();
      this.common.successToast('Artist updated successfully!');
      this.onAddEdit.emit(true);
    }, error => {
      this.flags.isUpdate = false;
    });
  }
  onCancel() {
    this.artistName = '';
    this.file = null;
    this.src = null;
      this.isfeaturedArtist=false
    $('#artistFile').val('');
    this.formData = new FormData();
    this.isEdit = false;
    document.getElementById('closeArtistModal').click();
  }
  error = message => {
    this.common.errorToast(message);
  }
      

}

