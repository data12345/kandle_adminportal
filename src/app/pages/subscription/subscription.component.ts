import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../../services/api/api.service';
import { Resp } from '../../models/Resp';
import { FilterBody } from '../../requests/filter-body';
import { UrlService } from '../../services/url/url.service';
import Swal from 'sweetalert2';
import { CommonService } from 'src/app/services/common/common.service';
@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.scss']
})
export class SubscriptionComponent implements OnInit {

  totalItems: number;
  serialNumber = 0;
  filterBody = new FilterBody();
  reportData: Array<any> = [];
  imageUrl: string;
  flags = {
    isEdit: false
  };
  config: any = {
    id: "page",
    currentPage: 1,
    itemsPerPage: 10
  };
  currentPage: Number;
  role: any;
  access: any;
  searchText: string;

  constructor(
    private api: ApiService,
    private url: UrlService,
    private common: CommonService,
  ) { }

  ngOnInit() {
    this.currentPage = 1;
    this.imageUrl = this.url.imageUrl;
    this.getContact(this.currentPage);
    if (JSON.stringify(localStorage.getItem('Kandle_Admin'))) {
      this.role = JSON.parse(localStorage.getItem('Kandle_Admin'))._value.role
      this.access = JSON.parse(localStorage.getItem('Kandle_Admin'))._value.access
    }
  }
  searchPlaylist() {
    this.filterBody.skip = 0;
    this.getContact(this.currentPage);
  }

  reset() {
    this.searchText = '';
    this.filterBody.searchText = undefined;
    this.filterBody.skip = 0;
    this.getContact(this.currentPage);
  }
  getContact(pageNo) {
    let data = {
      pageNo: pageNo,
    }
    
    this.api.getAllsubscription(data).subscribe((response: Resp) => {
      if (response.statusCode != 200) return;
      this.reportData = response.data.subscriptionHistory;
      this.totalItems = response.data.count;
      this.config = {
        id: "page",
        currentPage: pageNo,
        itemsPerPage: 10,
        totalItems: response.data.count
      };
    });
  }

  pageChange(page) {
    console.log(page)
    this.filterBody.skip = page.page - 1;
    this.serialNumber = this.filterBody.skip * this.filterBody.limit;
    this.getContact(page.page);
  }


}
