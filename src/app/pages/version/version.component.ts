import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { CommonService } from 'src/app/services/common/common.service';

@Component({
  selector: 'app-version',
  templateUrl: './version.component.html',
  styleUrls: ['./version.component.scss']
})
export class VersionComponent implements OnInit {
role:any
access:any
  versionData: any;
  flags = {
    isUpdate: false
  }

  constructor(private api: ApiService, private common: CommonService) { }

  ngOnInit() {
    if (JSON.stringify(localStorage.getItem('Kandle_Admin'))) {
      this.role = JSON.parse(localStorage.getItem('Kandle_Admin'))._value.role
      this.access = JSON.parse(localStorage.getItem('Kandle_Admin'))._value.access
    }
    this.getAppversions();

  }

  getAppversions() {
    this.api.getAppVersion().subscribe((response: any) => {
      console.log(response)
      this.versionData = response.data;
    })
  }

  setAppVersion() {
    let data = {
      contactUs: this.versionData.contactUs,
      criticalAndroidVersion: this.versionData.criticalAndroidVersion,
      criticalIOSVersion: this.versionData.criticalIOSVersion,
      criticalWebID: this.versionData.criticalWebID,
      isSpotifyEnable: true,
      latestAndroidVersion: this.versionData.latestAndroidVersion,
      latestIOSVersion: this.versionData.latestIOSVersion,
      latestWebID: this.versionData.latestWebID,
      privacyPolicy: this.versionData.privacyPolicy,
      termsAndConditions: this.versionData.termsAndConditions,
      updateMessageAtPopup: this.versionData.updateMessageAtPopup,
      updateTitleAtPopup: this.versionData.updateTitleAtPopup,
    }
    this.api.setAppVersion(data).subscribe((response: any) => {
      console.log(response)
      if (response.statusCode != 200)
        return this.common.errorToast(response.message)
      this.common.successToast(response.message);
      this.getAppversions();
    })
  }


}

