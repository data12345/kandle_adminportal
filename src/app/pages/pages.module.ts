import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UsersComponent } from './users/users.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {BsDatepickerModule, PaginationModule, TabsModule} from 'ngx-bootstrap';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { CategoryComponent } from './category/category.component';
import { CategoryModalComponent } from './category/category-modal/category-modal.component';
import {NgCircleProgressModule} from 'ng-circle-progress';
import {UiSwitchModule} from 'ngx-toggle-switch';
import {NgImageSliderModule} from 'ng-image-slider';
import {NumberOnlyDirective} from './directives/number-only/number-only.directive';
import { EditUserComponent } from './users/edit-user/edit-user.component';
import { AddUserComponent } from './users/add-user/add-user.component';
import { SettingComponent } from './setting/setting.component';
import { SubAdminComponent } from './sub-admin/sub-admin.component';
import { AddSubAdminComponent } from './sub-admin/add-sub-admin/add-sub-admin.component';
import { EditSubAdminComponent } from './sub-admin/edit-sub-admin/edit-sub-admin.component';
import { AdminProfileComponent } from './admin-profile/admin-profile.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';
import { NgSelect2Module } from 'ng-select2';
import { CharacterOnlyDirective } from './directives/character-only/character-only.directive';
import { BankdetailModalComponent } from './users/bankdetail-modal/bankdetail-modal.component';
import { DocumentModalComponent } from './users/document-modal/document-modal.component';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { MaterialModule } from '../modules/material/material.module';
import { CmsPagesComponent } from './cms-pages/cms-pages.component';
// import { AppversionComponent } from './appversion/appversion.component';

import {RichTextEditorAllModule  } from '@syncfusion/ej2-angular-richtexteditor';
import { FusionChartsModule } from 'angular-fusioncharts';
import * as TimeSeries from 'fusioncharts/fusioncharts.timeseries';

// Load FusionCharts
import * as FusionCharts from 'fusioncharts';
// Load Charts module
import * as Charts from 'fusioncharts/fusioncharts.charts';
// Load fusion theme
import * as FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
FusionChartsModule.fcRoot(FusionCharts, Charts, FusionTheme, TimeSeries);
import {NgxPaginationModule} from 'ngx-pagination';
import { VersionComponent } from './version/version.component';
import { ArtistComponent } from './artist/artist.component';
import { ArtistModalComponent } from './artist/artist-modal/artist-modal.component';
import { AlbumComponent } from './album/album.component';
import { AlbumModalComponent } from './album/album-modal/album-modal.component';
import { SongComponent } from './song/song.component';
import { SongModalComponent } from './song/song-modal/song-modal.component';
import { ViewSongModalComponent } from './song/view-song-modal/view-song-modal.component';
import { PlaylistComponent } from './playlist/playlist.component'; // <-- import the module
import { SelectDropDownModule } from "ngx-select-dropdown";
import { ArtistSongComponent } from './artist/artist-song/artist-song.component';
import { PlaylistModalComponent } from './playlist/playlist-modal/playlist-modal.component';
import { ArtistSongModalComponent } from './artist/artist-song/artist-song-modal/artist-song-modal.component';
import { ViewArtistSongComponent } from './artist/artist-song/view-artist-song/view-artist-song.component';
import { CelebrityComponent } from './celebrity/celebrity.component';
import { CelebrityModalComponent } from './celebrity/celebrity-modal/celebrity-modal.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { ReportComponent } from './report/report.component';
import { SubscriptionComponent } from './subscription/subscription.component';

@NgModule({
    declarations: [
        PagesComponent,
        DashboardComponent,
        UsersComponent,
        CategoryComponent,
        CategoryModalComponent,
        NumberOnlyDirective,
        EditUserComponent,
        AddUserComponent,
        VersionComponent,
        SettingComponent,
        SubAdminComponent,
        AddSubAdminComponent,
        EditSubAdminComponent,
        AdminProfileComponent,
        CharacterOnlyDirective,
        BankdetailModalComponent,
        DocumentModalComponent,
        CmsPagesComponent,
        VersionComponent,
        ArtistComponent,
        ArtistModalComponent,
        AlbumComponent,
        AlbumModalComponent,
        SongComponent,
        SongModalComponent,
        ViewSongModalComponent,
        PlaylistComponent,
        ArtistSongComponent,
        PlaylistModalComponent,
        ArtistSongModalComponent,
        ViewArtistSongComponent,
        CelebrityComponent,
        CelebrityModalComponent,
        ContactUsComponent,
        ReportComponent,
        SubscriptionComponent,
        // AppversionComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        FusionChartsModule,
        // MaterialModule,
        // MatChipsModule,
        SelectDropDownModule,
        PagesRoutingModule,
        PaginationModule.forRoot(),
        NgCircleProgressModule.forRoot(),
        UiSwitchModule,
        RichTextEditorAllModule,
        NgImageSliderModule,
        BsDatepickerModule.forRoot(),
        TabsModule.forRoot(),
        DragDropModule,
        NgMultiSelectDropDownModule.forRoot(),
        NgSelect2Module,
        GooglePlaceModule,
        MaterialModule,
        NgxPaginationModule
    ],
    exports: [
        NumberOnlyDirective
    ],
    entryComponents: [
        CategoryModalComponent,
    ]
})
export class PagesModule { }
