import { Component, OnInit, ViewChild } from '@angular/core';
import {ApiService} from '../../services/api/api.service';
import {Resp} from '../../models/Resp';
import {FilterBody} from '../../requests/filter-body';
import Swal from 'sweetalert2';
import { UrlService } from 'src/app/services/url/url.service';
import { CommonService } from 'src/app/services/common/common.service';
import { AlbumModalComponent } from './album-modal/album-modal.component';
import { album } from 'src/app/models/album';
@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.scss']
})
export class AlbumComponent implements OnInit {
 totalItems: number;
  serialNumber = 0;
  filterBody = new FilterBody();
  albumData: Array<album> = [];
  imageUrl: string;
  flags = {
    isEdit: false
  };
  config: any = {
    id: "page",
    currentPage: 1,
    itemsPerPage: 10
  };
  currentPage: Number;
  @ViewChild(AlbumModalComponent, {static: false}) public modalComponent: AlbumModalComponent;
  role: any;
  access: any;
  searchText: string;

  constructor(
    private api: ApiService,
    private url: UrlService,
    private common: CommonService,
  ) { }

  ngOnInit() {
    this.currentPage = 1;
    this.imageUrl = this.url.imageUrl;
    this.getAlbum(this.currentPage);
    if (JSON.stringify(localStorage.getItem('Kandle_Admin'))) {
      this.role = JSON.parse(localStorage.getItem('Kandle_Admin'))._value.role
      this.access = JSON.parse(localStorage.getItem('Kandle_Admin'))._value.access
    }
  }
  searchAlbum(){
    this.filterBody.skip=0;
    this.getAlbum(this.currentPage);
  }
    reset(){
    this.searchText='';
    this.filterBody.searchText=undefined;
    this.filterBody.skip=0;
    this.getAlbum(this.currentPage);
  } 
  getAlbum(pageNo) {
    let data={
      pageNo:pageNo}
      if (this.filterBody.searchText) {
        data['search'] = this.filterBody.searchText.trim();
    }
    this.api.getAllAlbum(data).subscribe((response: Resp) => {
      if (response.statusCode != 200) return;
      this.albumData = response.data.albumData;
      this.totalItems = response.data.count;
      this.config = {
        id: "page",
        currentPage: pageNo,
        itemsPerPage: 10,
        totalItems:response.data.count
      };
    });
  }
  deleteAlbum(id: string) {
    let body ={
      'albumId':id,
    }
    Swal.fire({
      title: 'Are you sure?',
      text: 'Once deleted, you will not be able to recover this Album!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085D6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      allowOutsideClick: false,
    }).then((result) => {
      if (result.value) {
        this.api.deleteAlbum(body).subscribe((response: Resp) => {
          if (response.statusCode !=200) return;
          Swal.fire({
            title: 'Deleted!',
            text: 'Poof! Your Album has been deleted!',
            icon: 'success'
          })
          this.getAlbum(this.currentPage);
        })
      }
    })
  }
  pageChange(page) {
    console.log(page)
    this.filterBody.skip = page.page - 1;
    this.serialNumber = this.filterBody.skip * this.filterBody.limit;
    this.getAlbum(page.page);
  }
  blockAlbum(item){
    let data={
      'albumId': item._id,
      'isBlocked':item.isBlocked
      }
      this.api.verifyBlockUnBlockAlbum(data).subscribe((response:any)=>{
        if(response.statusCode != 200) 
        return this.common.errorToast(response.message)
        this.common.successToast(response.message);
        this.getAlbum(this.currentPage);
      })
  }

}