import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ApiService } from '../../../services/api/api.service';
import { CommonService } from '../../../services/common/common.service';
import { Resp } from '../../../models/Resp';
declare var $: any;

@Component({
  selector: 'app-album-modal',
  templateUrl: './album-modal.component.html',
  styleUrls: ['./album-modal.component.scss']
})
export class AlbumModalComponent implements OnInit {
  @Input() isEdit: boolean;
  @Input() imageUrl: string;
  @Output() onAddEdit = new EventEmitter();
  src: any;
  file: File;
  formData = new FormData();
  albumName: string;
  artistType: Array<any> = [];
  artist: Array<any> = [];
  artist1: Array<any> = [];
  ArtistList: Array<any> = [];
  flags = {
    isAdded: false,
    isUpdate: false
  };
  image: any;
  albumId: any;
  dropDownSetting:any;
  singleDropDownSetting:any;
  albumData: any;
  constructor(
    private api: ApiService,
    private common: CommonService,
  ) { }

  ngOnInit() {
    this.dropDownSetting = this.common.dropSetting;
    this.singleDropDownSetting = this.common.singleDropSetting;
    this.getArtistList();
  }
    getArtistList() {
    const list = [];
    this.api.getAllArtistData('').subscribe((res: any) => {
      this.artistType = res.data.artistData
      res.data.artistData.forEach(ele => {
        list.push({
          item_id: ele._id,
          item_text: ele.name
        });
      });
      this.ArtistList = list;
      console.log(this.ArtistList);

    });
  }
  onImageSelect(e) {
    const file = e.target.files[0];
    this.formData.delete('image');
    if (file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/jpeg') {
      const reader = new FileReader();
      reader.onload = (event: any) => {
        this.src = event.target.result;
      };
      reader.readAsDataURL(e.target.files[0]);
      this.file = file;
      this.formData.append('image', this.file);
      this.api.uploadFileWithS3(this.formData).subscribe((res: any) => {
        if (res.statusCode == 200) {
          this.common.successToast(res['message'])
          this.image =  this.imageUrl + res.data.orignal;
          console.log(this.image,res.data)
        } else {
          this.common.errorToast(res['message'])
        }
      })
    } else {
      this.error('Selected file is not image.');
    }
  }
  onImageRemove() {
    this.src = null;
    $('#albumFile').val('');
  }
  onEditSelect(data) {
    this.albumId=  data._id
    let data1={
       "albumId":this.albumId }
      this.api.getAlbumByID(data1).subscribe((response: Resp) => {
        if (response.statusCode != 200) return;
        this.albumData = response.data;
        console.log(this.albumData)
        const list = [];
        if(this.albumData.artistData){
          console.log(this.albumData.artistData,response.data)
          this.albumData.artistData.forEach(ele => {
            console.log(ele,ele._id,this.artistType,ele.artistId._id,"------------------==")
        const obj = this.artistType.find(o => o._id.toString() == ele.artistId._id.toString());
        console.log(obj,"=========")
          if (obj) list.push({item_id: obj._id, item_text: obj.name});
         this.artist1.push(obj._id)
        });
        console.log(this.artist1)
        // this.artist1 = data.artistIds;
        this.artist=list
      }
        });
      this.albumName = data.name;
      this.isEdit = true;
      this.src = `${data.image}`;
      this.image= data.image;
   
    document.getElementById('openAlbumModal').click();
  }
  addAlbum() {
    if (!this.file) return this.error('Please select image first.');
    if (!this.artist1) return this.error('Please Select Artist.');
    this.flags.isAdded = true;
    let body = {
      "name": this.albumName,
      "image": this.image,
      "artistIds": this.artist1,

    }
    this.api.addAlbum(body).subscribe((response: Resp) => {
      this.flags.isAdded = false;
      if (response['statusCode'] != 200) {
        return this.common.errorToast(response.message);}
      this.onCancel();
      this.common.successToast('Album added successfully!');
      this.onAddEdit.emit(true);
    }, error => {
      this.flags.isAdded = false;
    });
  }
  editAlbum() {
    if (!this.artist1) return this.error('Please Select Artist.');
    this.flags.isUpdate = true;
    let body = {
      "name": this.albumName,
      "image": this.image,
      "albumId":this.albumId,
      "artistIds": this.artist1,

    }
    this.api.updateAlbum(body).subscribe((response: Resp) => {
      this.flags.isUpdate = false;
      if (response.statusCode != 200) return this.common.errorToast(response.message);
      this.onCancel();
      this.common.successToast('Album updated successfully!');
      this.onAddEdit.emit(true);
    }, error => {
      this.flags.isUpdate = false;
    });
  }
  onCancel() {
    this.albumName = '';
    this.file = null;
    this.src = null;
    this.artist1 = []
    this.artist = []
    $('#albumFile').val('');
    this.formData = new FormData();
    this.isEdit = false;
    document.getElementById('closeArtistModal').click();
  }
  error = message => {
    this.common.errorToast(message);
  }
  onItemSelect(item) {
    this.artist1.push(item.item_id);
    console.log(this.artist1, item)

  }
  onDeSelect(item: any) {
    console.log(item)
    let index
    index = this.artist1.indexOf(item.item_id);
    console.log(index)
    if (index > -1) 
    this.artist1.splice(index, 1);
  }
  onSelectAll(items: any) {
    console.log(items);
    items.forEach(ele => {
      this.artist1.push(ele.item_id);
      console.log(this.artist1)
    });
  }
}

