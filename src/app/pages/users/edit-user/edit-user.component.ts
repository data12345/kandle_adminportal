import { Component, OnInit, ViewChild } from '@angular/core';
import {AddUserBody} from "../../../requests/add-user-body";
import {ApiService} from "../../../services/api/api.service";
import {UrlService} from "../../../services/url/url.service";
import {CommonService} from "../../../services/common/common.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Resp} from "../../../models/Resp";
import countryCode from 'src/app/requests/countrycode';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
  history = window.history;
  body = new AddUserBody();
  userId: string;
  formData = new FormData();
  countryCode:any;
  codes=[];
  flags = {
    isUpdate: false
  };
  @ViewChild("placesRef", { static: false }) placesRef: GooglePlaceDirective;

  constructor(
    private api: ApiService,
    private common: CommonService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.codes = countryCode;
    this.activatedRoute.params.subscribe((param: any) => {
      this.userId = param.id;
      this.getUserDetail();
    });
  }
  getUserDetail() {
    this.api.getUserDetail({userId :this.userId}).subscribe((response: Resp) => {
      if (response.statusCode !=200) return;
      this.body = response.data;
      console.log(this.body,"=======")
    });
    

  }
  editUser() {
    this.flags.isUpdate = true;
    this.formData.append('firstName',this.body.firstName)
    this.formData.append('lastName',this.body.lastName)
    this.formData.append('email',this.body.email)
    this.formData.append('userName',this.body.userName)
    this.formData.append('countryCode',this.body.countryCode)
    this.formData.append('phoneNo',this.body.phoneNo)
    this.formData.append('location',this.body.location)
    this.formData.append('state',this.body.state)
    this.formData.append('city',this.body.city)
    this.formData.append('streetName',this.body.streetName)
    this.formData.append('zipCode',this.body.zipCode)
    // this.formData.append('gender',this.body.gender)
    this.formData.append('profileType',this.body.profileType)
    this.formData.append('latitude',this.body.latitude)
    this.formData.append('longitude',this.body.longitude)

    this.formData.append('userId',this.userId)
    this.api.editUser(this.formData).subscribe((response: Resp) => {
      this.flags.isUpdate = false;
      if (response.statusCode != 200) 
      return this.common.errorToast(response.message);
      this.common.successToast('User updated successfully!');
      this.router.navigateByUrl('/dashboard/users');
    }, error => {
      this.flags.isUpdate = false;
    });
  }
   public handleAddressChange(addressValue) {
    if (addressValue.formatted_address) {
      this.body.location = addressValue.formatted_address
      console.log( addressValue, "addesss")
    }
    addressValue.address_components.forEach(key => {
      if (key.types[0] == 'locality') {
        this.body.city = key.long_name
      }
      if (key.types[0] === 'administrative_area_level_1') {
        this.body.state = key.long_name
      }
      // if (key.types[0] === 'country') {
      //   this.body.Co = key.long_name
      // }
      if (key.types[0] == 'postal_code') {
        this.body.zipCode = key.long_name
        console.log(this.body.zipCode)

      }
      this.body.latitude = addressValue.geometry.location.lat()
      this.body.longitude = addressValue.geometry.location.lng()
       })
    
  }
 
}
