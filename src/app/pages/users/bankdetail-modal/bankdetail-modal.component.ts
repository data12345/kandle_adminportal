import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-bankdetail-modal',
  templateUrl: './bankdetail-modal.component.html',
  styleUrls: ['./bankdetail-modal.component.scss']
})
export class BankdetailModalComponent implements OnInit {
  @Input() BankList: Array<any>;

  constructor() { }

  ngOnInit() {
  }

}
