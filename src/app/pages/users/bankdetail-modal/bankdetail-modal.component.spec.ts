import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankdetailModalComponent } from './bankdetail-modal.component';

describe('BankdetailModalComponent', () => {
  let component: BankdetailModalComponent;
  let fixture: ComponentFixture<BankdetailModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankdetailModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankdetailModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
