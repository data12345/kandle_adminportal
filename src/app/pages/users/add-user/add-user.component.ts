import { Component, OnInit, ViewChild } from '@angular/core';
import {AddUserBody} from '../../../requests/add-user-body';
import {ApiService} from '../../../services/api/api.service';
import {CommonService} from '../../../services/common/common.service';
import {Resp} from "../../../models/Resp";
import {Router} from "@angular/router";
import countryCode from 'src/app/requests/countrycode';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {
  history = window.history;
  body = new AddUserBody();
    formData = new FormData();
  countryCode:any;
  codes=[];
  flags = {
    isAdded: false
  };
  @ViewChild("placesRef", { static: false }) placesRef: GooglePlaceDirective;


  constructor(
    private api: ApiService,
    private common: CommonService,
    private router: Router
  ) { }

  ngOnInit() {
    this.codes = countryCode;

  }
  addUser() {
    this.flags.isAdded = true;
    for (let key in this.body) {
      this.formData.append(`${key}`, this.body[key]);
      console.log(this.formData, `${key}`)
    }
    this.flags.isAdded = true;
    this.api.addUser(this.formData).subscribe((response: Resp) => {
      this.flags.isAdded = false;
      if (response.statusCode !=200 ){
        this.formData=new FormData();
        return this.common.errorToast(response.message);
        
    }
      this.common.successToast(response.message);
      this.router.navigateByUrl('/dashboard/users');
    }, error => {
      this.flags.isAdded = false;
    });
  }

  public handleAddressChange(addressValue) {
    if (addressValue.formatted_address) {
      this.body.location = addressValue.formatted_address
      console.log( addressValue, "addesss")
    }
    addressValue.address_components.forEach(key => {
      if (key.types[0] == 'locality') {
        this.body.city = key.long_name
      }
      if (key.types[0] === 'administrative_area_level_1') {
        this.body.state = key.long_name
      }
      // if (key.types[0] === 'country') {
      //   this.body.Co = key.long_name
      // }
      if (key.types[0] === 'postal_code') {
        this.body.zipCode = key.long_name
      }
      this.body.latitude = addressValue.geometry.location.lat()
      this.body.longitude = addressValue.geometry.location.lng()
       })
    
  }
}
