import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api/api.service';
import { CommonService } from 'src/app/services/common/common.service';
import { Resp } from 'src/app/models/Resp';
@Component({
  selector: 'app-cms-pages',
  templateUrl: './cms-pages.component.html',
  styleUrls: ['./cms-pages.component.scss']
})
export class CmsPagesComponent implements OnInit {

  form: FormGroup;
  form1:FormGroup;
  form2:FormGroup;
  form3:FormGroup
  private formSubmitAttempt: boolean;
  id: any;
  data: any;
  constructor(private fb: FormBuilder,private api: ApiService,
    private common:CommonService
    ) { }

  ngOnInit() {
    this.api.getStaticPages().subscribe(
        (data: any) => {
         if (data.data) {
           this.id=data.data._id
           this.data=data.data
           this.data.forEach(element => {
             if(element.slugName == 'terms-conditions')
             {
               console.log(element.description,"description")
               this.form = this.fb.group({
                TNC: element.description,
              });
              }
              if(element.slugName == 'about-us')
              {
                console.log(element.description,"description")
                this.form1 = this.fb.group({
                  aboutUs: element.description,
               });
               }
               if(element.slugName == 'privacy-policy')
               {
                 console.log(element.description,"description")
                 this.form3 = this.fb.group({
                  privacy: element.description,
                });
                }
           });
                      
          }
        })

    this.form = this.fb.group({
      TNC: ['', Validators.required]
    });
    this.form1 = this.fb.group({
      aboutUs: ['', Validators.required]
    });
    this.form2 = this.fb.group({
      contactUs: ['', Validators.required]
    });
    this.form3 = this.fb.group({
      privacy: ['', Validators.required]
    });
  }
  isFieldInvalid(field: string) {
    return (
      (!this.form.get(field).valid && this.form.get(field).touched) ||
      (this.form.get(field).untouched && this.formSubmitAttempt)
    );
  }
  onSubmit(){
    if (this.form.valid) {
       let form = {
        title: 'Terms & Conditions',
        description: this.form.value.TNC
      }
  this.api.saveStaticPages(form).subscribe(
           (response: Resp) => {
            if (response.statusCode != 200)return
            this.common.successToast('Updated successfully!');
      })
          }
    this.formSubmitAttempt = true;
  }

  onSubmit1(){
    if (this.form1.valid) {
      let form = {
        title: 'About Us',
        description: this.form1.value.aboutUs
     }
       this.api.saveStaticPages(form).subscribe(
          (response: Resp) => {
            if (response.statusCode != 200)return
            this.common.successToast('Updated successfully!');
          })
         }
       this.formSubmitAttempt = true;
 }
 onSubmit2(){
  if (this.form2.valid) {
    let form = {
     id: this.id,
     contactUs: this.form2.value.contactUs
   }
    // this.api.saveStaticPages(form).subscribe(
    //    (response: Resp) => {
    //      if (!response.success)return
    //     this.common.successToast('Updated successfully!');
    //    })
       }
     this.formSubmitAttempt = true;
}
onSubmit3(){
  if (this.form3.valid) {
    let form = {
      title: 'Privacy & Policy',
      description : this.form3.value.privacy
   }
    this.api.saveStaticPages(form).subscribe(
       (response: Resp) => {
         if (response.statusCode != 200)return
        this.common.successToast('Updated successfully!');
        // this.ngOnInit();
       })
       }
     this.formSubmitAttempt = true;
}
  }

