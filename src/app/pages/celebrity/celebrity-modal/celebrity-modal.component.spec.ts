import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CelebrityModalComponent } from './celebrity-modal.component';

describe('CelebrityModalComponent', () => {
  let component: CelebrityModalComponent;
  let fixture: ComponentFixture<CelebrityModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CelebrityModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CelebrityModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
