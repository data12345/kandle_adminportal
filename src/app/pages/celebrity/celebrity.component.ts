import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../services/api/api.service';

import {UrlService} from '../../services/url/url.service';

import {Resp} from '../../models/Resp';
import {UserList} from '../../models/user-list';
import {FilterBody} from '../../requests/filter-body';
import { AngularCsv } from 'angular7-csv';
import Swal from 'sweetalert2';
import { AddUserBody } from 'src/app/requests/add-user-body';
import { CommonService } from 'src/app/services/common/common.service';
declare var swal: any;
@Component({
  selector: 'app-celebrity',
  templateUrl: './celebrity.component.html',
  styleUrls: ['./celebrity.component.scss']
})
export class CelebrityComponent implements OnInit {

  totalItems: number;
  selectedProduct:any
  body :any;
  config: any = {
    id: "page",
    currentPage: 1,
    itemsPerPage: 10
  };
  imageUrl:string;
  currentPage: Number;
  filterBody = new FilterBody();
  userList: Array<UserList> = [];
  role: any;
  access: any;
  userListData: any;
  serialNumber = 0;
  searchText: any;
  constructor(
    private api: ApiService,
    private common: CommonService,
   private url:UrlService
  ) { }

  ngOnInit() {
    this.currentPage = 1;
    this.imageUrl= this.url.imageUrl
    this.filterBody.searchText=''
    this.getUserList(this.currentPage);
    if (JSON.stringify(localStorage.getItem('Kandle_Admin'))) {
      this.role = JSON.parse(localStorage.getItem('Kandle_Admin'))._value.role
      this.access = JSON.parse(localStorage.getItem('Kandle_Admin'))._value.access
    }
  }
  getUserList(pageNo) {
    let data={
      pageNo:pageNo}
      if (this.filterBody.searchText) {
        data['search'] = this.filterBody.searchText.trim();
    }
    this.api.getCelebrityDetail(data).subscribe((response: Resp) => {
      if (response.statusCode != 200) return;
      this.userList = response.data.userData;
      this.totalItems = response.data.count;  
      console.log(this.userList,"adata")
      this.config = {
        id: "page",
        currentPage: pageNo,
        itemsPerPage: 10,
        totalItems:response.data.count
      };

    });
  }
   RemoveCelebrity(id: string) {
    let body ={
      'userId':id,
    }
    Swal.fire({
      title: 'Are you sure?',
      text: 'Once Removed, you will not be able to recover this Celebrity!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085D6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      allowOutsideClick: false,
    }).then((result) => {
      if (result.value) {
        this.api.deleteCelebrity(body).subscribe((response: Resp) => {
          if (response.statusCode != 200) return;
          Swal.fire({
            title: 'Deleted!',
            text: 'Poof! Your Celebrity has been Removed!',
            icon: 'success'
          })
          this.getUserList(this.currentPage);
        })
      }
    })
  }
 
  approved(data,item){
    console.log("dtaa",data)
    let body={
      'formId':data._id,
      'status':item,
    }
    this.api.CelebrityStatus(body).subscribe((response: Resp) => {
      if (response.statusCode !=200) return;
      if(response.statusCode == 200)
     this.common.successToast(response.message)
      this.getUserList(this.currentPage);
    })
  }
  Block(data){
    console.log("dtaa",data)
    let body={
      'userId':data.id,
      'isBlocked':true,
    }
    Swal.fire({
      title: 'Are you sure,?',
      text: 'you want to block this user!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085D6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      allowOutsideClick: false,
    }).then((result) => {
      if (result.value) {
        this.api.deleteUser(body).subscribe((response: Resp) => {
          if (response.statusCode !=200) return;
          Swal.fire({
            title: 'Blocked!',
            text: 'Poof! Your User has been Blocked!',
            icon: 'success'
          })
          this.getUserList(this.currentPage);
          // this.ngOnInit()
        })
      }
    })
  }
  onPageChange(page) {
    this.filterBody.skip = page.page - 1;
    this.serialNumber = this.filterBody.skip * this.filterBody.limit;
    this.getUserList(page.page);
  }
  searchUser(){
    // this.filterBody.searchText=this.searchText.trim();
    this.filterBody.skip=0;
    this.getUserList(this.currentPage);
  }

  reset(){
    this.searchText='';
    this.filterBody.searchText=undefined;
    this.filterBody.skip=0;
    this.getUserList(this.currentPage);
  }

}
