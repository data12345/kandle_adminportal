import {Component, OnInit, ViewChild} from '@angular/core';
import {ApiService} from '../../services/api/api.service';
import {Resp} from '../../models/Resp';
import {FilterBody} from '../../requests/filter-body';
import {Categories} from '../../models/categories';
import {UrlService} from '../../services/url/url.service';
import {CategoryModalComponent} from "./category-modal/category-modal.component";
declare var swal: any;
import Swal from 'sweetalert2';
import { CommonService } from 'src/app/services/common/common.service';
import { listenToTriggers } from 'ngx-bootstrap/utils';
@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
  totalItems: number;
  serialNumber = 0;
  filterBody = new FilterBody();
  categoryList: Array<Categories> = [];
  imageUrl: string;
  flags = {
    isEdit: false
  };
  config: any = {
    id: "page",
    currentPage: 1,
    itemsPerPage: 10
  };
  currentPage: Number;
  @ViewChild(CategoryModalComponent, {static: false}) public modalComponent: CategoryModalComponent;
  role: any;
  access: any;
  searchText: string;

  constructor(
    private api: ApiService,
    private url: UrlService,
    private common: CommonService,
  ) { }

  ngOnInit() {
    this.currentPage = 1;
    this.imageUrl = this.url.imageUrl;
    this.getCategories(this.currentPage);
    if (JSON.stringify(localStorage.getItem('Kandle_Admin'))) {
      this.role = JSON.parse(localStorage.getItem('Kandle_Admin'))._value.role
      this.access = JSON.parse(localStorage.getItem('Kandle_Admin'))._value.access
    }
  }
  searchCataegory(){
    this.filterBody.skip=0;
    this.getCategories(this.currentPage);
  }

  reset(){
    this.searchText='';
    this.filterBody.searchText=undefined;
    this.filterBody.skip=0;
    this.getCategories(this.currentPage);
  }
  getCategories(pageNo) {
    let data={
      pageNo:pageNo}
      if (this.filterBody.searchText) {
        data['search'] = this.filterBody.searchText.trim();
    }
    this.api.getAllCategories(data).subscribe((response: Resp) => {
      if (response.statusCode !=200) return;
      this.categoryList = response.data.categoryData;
      this.totalItems = response.data.count;
      this.config = {
        id: "page",
        currentPage: pageNo,
        itemsPerPage: 10,
        totalItems:response.data.count
      };
    });
  }
  deleteCategory(id: string) {
    let body ={
      'categoryId':id,
    }
    Swal.fire({
      title: 'Are you sure?',
      text: 'Once deleted, you will not be able to recover this Category!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085D6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      allowOutsideClick: false,
    }).then((result) => {
      if (result.value) {
        this.api.deleteCategory(body).subscribe((response: Resp) => {
          if (response.statusCode !=200) return;
          Swal.fire({
            title: 'Deleted!',
            text: 'Poof! Your User has been deleted!',
            icon: 'success'
          })
          this.getCategories(this.currentPage);
          // this.ngOnInit()
        })
      }
    })
  }
  pageChange(page) {
    console.log(page)
    this.filterBody.skip = page.page - 1;
    this.serialNumber = this.filterBody.skip * this.filterBody.limit;
    this.getCategories(page.page);
  }
  blockCategory(item){
    let data={
      'categoryId': item._id,
      'isBlocked':item.isBlocked
      }
      this.api.verifyBlockUnBlockCategory(data).subscribe((response:any)=>{
        if(response.statusCode !=200) 
       return this.common.errorToast(response.message)
        this.common.successToast(response.message);
        this.getCategories(this.currentPage);
      })
  }

}
