import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ApiService } from '../../../services/api/api.service';
import { CommonService } from '../../../services/common/common.service';
import { Resp } from '../../../models/Resp';
declare var $: any;

@Component({
  selector: 'app-category-modal',
  templateUrl: './category-modal.component.html',
  styleUrls: ['./category-modal.component.scss']
})
export class CategoryModalComponent implements OnInit {
  @Input() isEdit: boolean;
  @Input() imageUrl: string;
  @Output() onAddEdit = new EventEmitter();
  src: any;
  file: File;
  formData = new FormData();
  categoryName: string;
  flags = {
    isAdded: false,
    isUpdate: false
  };
  image: any;
  categoryId: any;
  isSoportify: any;
  constructor(
    private api: ApiService,
    private common: CommonService,
  ) { }

  ngOnInit() {
  }
  onImageSelect(e) {
    const file = e.target.files[0];
    this.formData.delete('image');
    if (file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/jpeg') {
      const reader = new FileReader();
      reader.onload = (event: any) => {
        this.src = event.target.result;
      };
      reader.readAsDataURL(e.target.files[0]);
      this.file = file;
      this.formData.append('image', this.file);
      this.api.uploadFileWithS3(this.formData).subscribe((res: any) => {
        if (res.statusCode == 200) {
          this.common.successToast(res['message'])
          this.image =  res.data.orignal;
          console.log(this.image,res.data)
        } else {
          this.common.errorToast(res['message'])
        }
      })
    } else {
      this.error('Selected file is not image.');
    }
  }
  onImageRemove() {
    this.src = null;
    $('#categoryFile').val('');
  }
  onEditSelect(data) {
    this.categoryName = data.name;
    this.isEdit = true;
    this.src =  this.imageUrl +`${data.image}`;
    this.image= data.image;
    this.isSoportify = data.isSoportify
    this.categoryId=  data._id
    this.formData.append('id', data._id);
    document.getElementById('openCategoryModal').click();
  }
  addCategory() {
    if (!this.file) return this.error('Please select image first.');
    this.flags.isAdded = true;
    let body = {
      "name": this.categoryName,
      "image": this.image,
      "country": 'en',
      'locale': 'en'
    }
    this.api.addCategory(body).subscribe((response: Resp) => {
      this.flags.isAdded = false;
      if (response['statusCode'] != 200) {
        return this.common.errorToast(response.message);}
      this.onCancel();
      this.common.successToast('Category added successfully!');
      this.onAddEdit.emit(true);
    }, error => {
      this.flags.isAdded = false;
    });
  }
  editCategory() {
    this.flags.isUpdate = true;
    let body = {
      "name": this.categoryName,
      "image": this.image,
      "categoryId":this.categoryId,
      "country": 'en',
      'locale': 'en'
    }
    this.api.updateCategory(body).subscribe((response: Resp) => {
      this.flags.isUpdate = false;
      if (response.statusCode != 200) return this.common.errorToast(response.message);
      this.onCancel();
      this.common.successToast('Category updated successfully!');
      this.onAddEdit.emit(true);
    }, error => {
      this.flags.isUpdate = false;
    });
  }
  onCancel() {
    this.categoryName = '';
    this.file = null;
    this.src = null;
    $('#categoryFile').val('');
    this.formData = new FormData();
    this.isEdit = false;
    document.getElementById('closeCategoryModal').click();
  }
  error = message => {
    this.common.errorToast(message);
  }

}
