import { TestBed } from '@angular/core/testing';

import { SrcService } from './src.service';

describe('SrcService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SrcService = TestBed.get(SrcService);
    expect(service).toBeTruthy();
  });
});
