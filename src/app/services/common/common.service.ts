import { Injectable } from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';
import {ToastrManager} from 'ng6-toastr-notifications';
import {ApiService} from '../api/api.service';
import {Resp} from '../../models/Resp';
import { Observable, BehaviorSubject } from 'rxjs';
import {LocalStorageService} from 'angular-web-storage';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  dropSetting = {
    enableCheckAll: false,
    singleSelection: false,
    idField: 'item_id',
    textField: 'item_text',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 3,
    allowSearchFilter: true
  };
  singleDropSetting = {
    enableCheckAll: false,
    singleSelection: true,
    idField: 'item_id',
    textField: 'item_text',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 3,
    allowSearchFilter: true
  };
  username: string;
  adminName: BehaviorSubject<string>;

  constructor(
    private spinner: NgxSpinnerService,
    private toaster: ToastrManager,
    private api: ApiService,
    private localStorage: LocalStorageService
  ) { 
    if(localStorage.get("admin_name")){
      const adminName = localStorage.get("admin_name");
      console.log(adminName,"name")
      this.adminName = new BehaviorSubject<string>(adminName);
    }
  }
  showSpinner() {
    this.spinner.show();
  }
  hideSpinner() {
    this.spinner.hide();
  }
  successToast(message) {
    this.toaster.successToastr(message, '', {
      maxShown: 1
    });
  }
  errorToast(message) {
    this.toaster.errorToastr(message);
  }
  getCategories() {
    return new Promise((resolve, reject) => {
      this.api.getCategories().subscribe((response: Resp) => {
        if (!response.success) return;
        return resolve(response);
      });
    });
  }
  // getSubCategories(id: string) {
  //   return new Promise((resolve, reject) => {
  //     this.api.getSubCatWithoutPagination(id).subscribe((response: Resp) => {
  //       if (!response.success) return;
  //       return resolve(response);
  //     });
  //   });
  // }
  // getProductBySubCat(id: string) {
  //   return new Promise((resolve, reject) => {
  //     this.api.getSubCatProduct(id).subscribe((response: Resp) => {
  //       if (!response.success) return;
  //       return resolve(response);
  //     });
  //   });
  // }
  async getSubCatDetails(id: string) {
    return new Promise((resolve, reject) => {
      this.api.getSubCatDetails(id).subscribe((response: Resp) => {
        if (!response.success) return;
        return resolve(response.data);
      });
    });
  }

  geAdminName(){
    return this.adminName.asObservable();
  }

  setgetAdminName() {
    if(this.localStorage.get("admin_name")){
      let name= this.localStorage.get('admin_name');
      console.log(name,"name")
      this.adminName.next(name);
    }
  }

}
