import { Injectable } from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {CommonService} from '../common/common.service';
import {LocalStorageService} from "angular-web-storage";

@Injectable({
  providedIn: 'root'
})
export class GetInterceptorService {

  constructor(
    private common: CommonService,
    private router: Router,
    private localStorage: LocalStorageService
  ) { }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(tap((event: HttpEvent<any>) => {
      console.log(event,"event")
      if (event instanceof HttpResponse) {
        this.common.hideSpinner();
              console.log(event,"event111")
                if (event['status'] == 211) {
                console.log("message error")
                  this.common.errorToast('Your session is expired, please log in.');
                  localStorage.clear();
                  return this.router.navigateByUrl('/');
                }
        // if (event.status == 200 && !event.body.success) this.error(event.body.message);
      }
    }, (error: any) => {

      if (error instanceof HttpErrorResponse) {
        this.common.hideSpinner();
        console.log("error",error)
        if (event['status'] == 211)      {
              // this.error('Your session is expired, please sign in.');
          this.localStorage.clear();
          return this.router.navigateByUrl('/login');
        } else {
          // return alert(error.message);
        }
      }
    }));
  }
  error = message => {
    // this.common.errorToast(message);
  }
}
