import { Injectable } from '@angular/core';
import { FilterBody } from 'src/app/requests/filter-body';

@Injectable({
  providedIn: 'root'
})
export class UrlService {
  baseUrl = 'http://appgrowthcompany.com:3084/api/v1';
   imageUrl = 'http://appgrowthcompany.com:3084';

  constructor() { }
  login = `${this.baseUrl}/admin/login`;
  getPagesUrl =`${this.baseUrl}/admin/getAllStaticPages`;
  dashboard=`${this.baseUrl}/admin/getDashboardCount`;
  dashboardgraph=`${this.baseUrl}/admin/getDashboardGraph`;
  getProfileUrl = `${this.baseUrl}/admin/getProfile`;
  updateProfileUrl = `${this.baseUrl}/admin/updateAdminProfile`;
  updatePagesUrl = `${this.baseUrl}/admin/addStaticPage`;
  changePasswordUrl = `${this.baseUrl}/admin/changePassword`;
  forgotChangePassword =`${this.baseUrl}/admin/forgotChangePassword`;
  getAllUserUrl = `${this.baseUrl}/admin/getAllUserProfile`;
  deleteUserUrl = `${this.baseUrl}/admin/deleteBlockUnBlockDeactivateUserProfile`;
  getUsersUrl = `${this.baseUrl}/admin/getUserProfile`;
  getAddUserUrl  = `${this.baseUrl}/admin/addUser`;
  upadeteUserUrl  = `${this.baseUrl}/admin/updateUserProfile`;
  getCelebrityUrl = `${this.baseUrl}/admin/getAllCelebrityForm`;
  updateCelebrityUrl = `${this.baseUrl}/admin/acceptAndRejectCelebrityForm`;
  celebrityUrl=`${this.baseUrl}/admin/removeCelebrity`
  getAllCategoriesUrl = `${this.baseUrl}/admin/getAllCategory`;
  getAllCategoriesDataUrl= `${this.baseUrl}/admin/getAllCategoryWOP`;
  categoryUrl = `${this.baseUrl}/admin/deleteCategory`;
  addcategoryUrl = `${this.baseUrl}/admin/addCategory`;
  editcategoryUrl = `${this.baseUrl}/admin/editCategory`;
  blockcategoryUrl = `${this.baseUrl}/admin/blockUnblockCategory`;
  getAllPlaylistUrl = `${this.baseUrl}/admin/getAllPlaylist`;
  playlistUrl = `${this.baseUrl}/admin/deletePlaylist`;
  addPlaylistUrl = `${this.baseUrl}/admin/addPlaylist`;
  editPlaylistUrl = `${this.baseUrl}/admin/editPlaylist`;
  blockPlaylistUrl = `${this.baseUrl}/admin/blockUnblockPlaylist`;
  getAllContactUrl = `${this.baseUrl}/admin/getAllContacts`;
  getAllReportUrl  = `${this.baseUrl}/admin/getAllReport`;
  valueUrl =`${this.baseUrl}/admin/get_options`;
  uploadFileUrl = `${this.baseUrl}/admin/uploadFile`;
  appversionUrl = `${this.baseUrl}/admin/getAppVersion`;
  setAppVersionUrl = `${this.baseUrl}/admin/setAppVersion`;
  getAllArtistUrl = `${this.baseUrl}/admin/getAllArtist`;
  getAllsubscriptionUrl = `${this.baseUrl}/admin/getSubscriptionHistory`;
  getAllArtistdataUrl = `${this.baseUrl}/admin/getAllArtistWOP `;
  getAllPlayListdataUrl = `${this.baseUrl}/admin/getAllPlaylistWOP  `;
  artistUrl = `${this.baseUrl}/admin/deleteArtist`;
  addArtistUrl = `${this.baseUrl}/admin/addArtist`;
  editArtistUrl = `${this.baseUrl}/admin/editArtist`;
  blockArtistUrl = `${this.baseUrl}/admin/blockUnblockArtist`;
  // getAllAttributeUrl =`${this.baseUrl}/admin/options?limit=${FilterBody}&page=${FilterBody}`;
  getAllSongUrl = `${this.baseUrl}/admin/getAllSong`
  getSongDetailUrl = `${this.baseUrl}/admin/getSongDetails`
  songUrl = `${this.baseUrl}/admin/deleteSong`;
  addSongUrl = `${this.baseUrl}/admin/addSong`;
  editSongUrl = `${this.baseUrl}/admin/editSong`;
  blockSongUrl = `${this.baseUrl}/admin/blockUnblockSong`;
  getAllAlbumUrl = `${this.baseUrl}/admin/getAllAlbum`;
  getAlbumUrlById = `${this.baseUrl}/admin/getAlbumDetails`;
  getAllArtistById = `${this.baseUrl}/admin/getAllAlbumWithArtistWOP`;
  getAllAlbumDataUrl = `${this.baseUrl}/admin/getAllAlbumWOP `
  albumUrl = `${this.baseUrl}/admin/deleteAlbum`;
  addAlbumUrl = `${this.baseUrl}/admin/addAlbum`;
  editAlbumUrl = `${this.baseUrl}/admin/editAlbum`;
  blockAlbumUrl = `${this.baseUrl}/admin/blockUnblockAlbum`;
  resetPasswordUrl = `${this.baseUrl}/admin/resetPassword`;
  getAllOrdersUrl = `${this.baseUrl}/admin/getAllOrders`;
  getAllSubCategories = `${this.baseUrl}/admin/getSubCategories`;
  getAllSubAdminUrl = `${this.baseUrl}/admin/getAllSubAdminProfile`;
  subCategoryUrl = `${this.baseUrl}/admin/subCategory`;
  forgotPasswordUrl = `${this.baseUrl}/admin/forgotPassword`;
  settingUrl = `${this.baseUrl}/admin/setting`;
  subAdminUrl = `${this.baseUrl}/admin/addSubAdmin`;
  uploadImageUrl = `${this.baseUrl}/admin/uploadProductImage`;
  getAdminDetailsUrl = id => `${this.baseUrl}/admin/getAdminDetails/${id}`;
  deleteCategoryUrl = id => `${this.categoryUrl}`;
  deleteSubCategoryUrl = id => `${this.subCategoryUrl}?id=${id}`;
  deleteSubAdminUrl =  `${this.baseUrl}/admin/deleteSubAdmin`;
editsubadminurl =  `${this.baseUrl}/admin/updateSubAdmin`;
  getSubCatDetail = id => `${this.baseUrl}/admin/getSubCatDetail/${id}`;
  getExportUser = id => `${this.baseUrl}/admin/getAllUserProfileExport?accessToken=${id}`;

}
