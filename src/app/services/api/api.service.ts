import { Injectable } from '@angular/core';
import { HttpClient, HttpEventType, HttpHeaders } from '@angular/common/http';
import { UrlService } from '../url/url.service';
import { LoginBody } from '../../requests/login-body';
import { FilterBody } from '../../requests/filter-body';
import { map } from 'rxjs/operators';
import { ProductBody } from '../../requests/product-body';
import { CouponBody } from '../../requests/coupon-body';
import { AddUserBody } from '../../requests/add-user-body';
import { AddSubAdminBody } from '../../requests/add-sub-admin-body';
import { DriverBody } from '../../requests/driver-body';
import { GiftCardBody } from 'src/app/requests/giftcard-body copy';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  tokenVal: any;

  constructor(

    private http: HttpClient,
    private url: UrlService
  ) { 
  
  }
  singIn(body: LoginBody) {
    return this.http.post(this.url.login, body);
  }
  getDahboard() {
    return this.http.get(this.url.dashboard);
  }
  getDahboardGraph(body) {
    return this.http.post(this.url.dashboardgraph, body);
  }
  forgotPassword(body: any) {
    return this.http.post(this.url.forgotPasswordUrl, body);
  }
  changePassword(body: any) {
    return this.http.post(this.url.changePasswordUrl, body);
  }
  getProfile(body) {
    return this.http.post(this.url.getProfileUrl,body);
  }
  getUsers(body) {
    return this.http.post(this.url.getAllUserUrl, body);
  }
  deleteUser(id) {
    return this.http.post(this.url.deleteUserUrl,id);
  }
  addUser(body:FormData) {
    return this.http.post(this.url.getAddUserUrl, body);
  }
  getUserDetail(body) {
    return this.http.post(this.url.getUsersUrl,body);
  }
  getUserExportDetail(id) {
    return this.http.get(this.url.getExportUser(id));
  }
  editUser(body:FormData) {
    return this.http.post(this.url.upadeteUserUrl, body);
  }
  getCelebrityDetail(body) {
    return this.http.post(this.url.getCelebrityUrl,body);
  }
  CelebrityStatus(body) {
    return this.http.post(this.url.updateCelebrityUrl,body);
  }
  getAllCategories(body) {
    return this.http.post(this.url.getAllCategoriesUrl, body);
  }
  getAllCategoriesData(body) {
    return this.http.post(this.url.getAllCategoriesDataUrl, body);
  }
  addCategory(body) {
    return this.http.post(this.url.addcategoryUrl, body);
  }
  deleteCategory(id) {
    return this.http.post(this.url.categoryUrl,id);
  }
    deleteCelebrity(id) {
    return this.http.post(this.url.celebrityUrl,id);
  }
  updateCategory(body) {
    return this.http.post(this.url.editcategoryUrl, body);
  }
  verifyBlockUnBlockCategory(body) {
    return this.http.post(this.url.blockcategoryUrl, body);
  }

  uploadFileWithS3(body:FormData) {
    // var reqHeader = new HttpHeaders({ 'Authorization':this.tokenVal });
    // return this.http.post(this.url.uploadFileUrl, body,{headers: reqHeader })
    return this.http.post(this.url.uploadFileUrl, body);
  }
  getAllplaylist(body) {
    return this.http.post(this.url.getAllPlaylistUrl, body);
  }
 
  addPlaylist(body) {
    return this.http.post(this.url.addPlaylistUrl, body);
  }
  deletePlaylist(id) {
    return this.http.post(this.url.playlistUrl,id);
  }
  updatePlaylist(body) {
    return this.http.post(this.url.editPlaylistUrl, body);
  }
  verifyBlockUnBlockPlaylist(body) {
    return this.http.post(this.url.blockPlaylistUrl, body);
  }

  getAppVersion() {
    return this.http.get(this.url.appversionUrl);
  }
  setAppVersion(body) {
    return this.http.post(this.url.setAppVersionUrl,body);
  }
  getAllContact(body) {
    return this.http.post(this.url.getAllContactUrl, body);
  }
   getAllReport(body) {
    return this.http.post(this.url.getAllReportUrl, body);
  }
  getAllsubscription(body) {
    return this.http.post(this.url.getAllsubscriptionUrl, body);
  }
  getAllArtist(body) {
    return this.http.post(this.url.getAllArtistUrl, body);
  }
  getAllArtistData(body) {
    return this.http.post(this.url.getAllArtistdataUrl, body);
  }
  addArtist(body) {
    return this.http.post(this.url.addArtistUrl, body);
  }
  deleteArtist(id) {
    return this.http.post(this.url.artistUrl,id);
  }
  updateArtist(body) {
    return this.http.post(this.url.editArtistUrl, body);
  }
  verifyBlockUnBlockArtist(body) {
    return this.http.post(this.url.blockArtistUrl, body);
  }
  getStaticPages() {
    return this.http.get(this.url.getPagesUrl);
  }
  saveStaticPages(body: any) {
    return this.http.post(this.url.updatePagesUrl, body);
  }
  getAllSong(body) {
    return this.http.post(this.url.getAllSongUrl, body);
  }
  getSongDetail(body) {
    return this.http.post(this.url.getSongDetailUrl, body);
  }
  addSong(body) {
    return this.http.post(this.url.addSongUrl, body);
  }
  deleteSong(id) {
    return this.http.post(this.url.songUrl,id);
  }
  updateSong(body) {
    return this.http.post(this.url.editSongUrl, body);
  }
  verifyBlockUnBlockSong(body) {
    return this.http.post(this.url.blockSongUrl, body);
  }
  getAllAlbum(body) {
    return this.http.post(this.url.getAllAlbumUrl, body);
  }
  getAlbumByID(body) {
    return this.http.post(this.url.getAlbumUrlById, body);
  }
  getAllAlbumData(body) {
    return this.http.post(this.url.getAllAlbumDataUrl, body);
  }
  getAllArtistById(body) {
    return this.http.post(this.url.getAllArtistById, body);
  }
  addAlbum(body) {
    return this.http.post(this.url.addAlbumUrl, body);
  }
  deleteAlbum(id) {
    return this.http.post(this.url.albumUrl,id);
  }
  updateAlbum(body) {
    return this.http.post(this.url.editAlbumUrl, body);
  }
  verifyBlockUnBlockAlbum(body) {
    return this.http.post(this.url.blockAlbumUrl, body);
  }
  getSubCat(body: FilterBody) {
    return this.http.post(this.url.getAllSubCategories, body);
  }
  addSubCat(body: FormData) {
    return this.http.post(this.url.subCategoryUrl, body);
  }
  editSubCat(body: FormData) {
    return this.http.put(this.url.subCategoryUrl, body);
  }
  deleteSubCat(id: string) {
    return this.http.delete(this.url.deleteSubCategoryUrl(id));
  }
  getCategories() {
    return this.http.get(this.url.categoryUrl);
  }
  getValues() {
    return this.http.get(this.url.valueUrl);
  }
  addSetting(body: any) {
    return this.http.post(this.url.settingUrl, body);
  }
  editSetting(body: any) {
    return this.http.put(this.url.settingUrl, body);
  }
  getSetting() {
    return this.http.get(this.url.settingUrl);
  }
  addSubAdmin(body: AddSubAdminBody) {
    return this.http.post(this.url.subAdminUrl, body);
  }
  editSubAdmin(body: any) {
    return this.http.post(this.url.editsubadminurl, body);
  }
  deleteSubAdmin(id: any) {
    return this.http.post(this.url.deleteSubAdminUrl,id);
  }
  getAllSubAdmin(body: any) {
    return this.http.post(this.url.getAllSubAdminUrl, body);
  }
  getAdminDetails(body:any) {
    return this.http.post(this.url.getAllSubAdminUrl,body);
  }
  
  updateProfile(body: any) {
    return this.http.post(this.url.updateProfileUrl, body);
  }
  resetChangePassword(body){
    return this.http.post(this.url.forgotChangePassword,body);
  }
  
  getSubCatDetails(id: string) {
    return this.http.get(this.url.getSubCatDetail(id));
  }
  
  resetPassword(body: any) {
    return this.http.post(this.url.resetPasswordUrl, body);
  }
 
  uploadImage(body: FormData) {
    return this.http.post(this.url.uploadImageUrl, body, {
      reportProgress: true,
      observe: 'events'
    }).pipe(map((event: any) => {
      switch (event.type) {

        case HttpEventType.UploadProgress:
          const progress = Math.round(100 * event.loaded / event.total);
          return { success: true, message: 'progress', data: progress };

        case HttpEventType.Response:
          return event.body;
        default:
          return `Unhandled event: ${event.type}`;
      }
    }));
  }
  getAllPlaytListData(body){
    return this.http.post(this.url.getAllPlayListdataUrl, body);

  }
}
