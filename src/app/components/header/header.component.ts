import { Component, OnInit } from '@angular/core';
import {Admin} from "../../models/Admin";
import {LocalStorageService} from "angular-web-storage";
import {CommonService} from "../../services/common/common.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  user = new Admin();
  adminName: string;

  constructor(
    private localStorage: LocalStorageService,
    public common: CommonService
  ) { }

  ngOnInit() {
    this.user = this.localStorage.get('Kandle_Admin');
    this.adminName = this.localStorage.get('admin_name');
    this.common.geAdminName().subscribe((res :any)=>{
      console.log(res,"-----")
      this.common.username = res;
      console.log(this.common.username ,"this.common.username ")
    })
  }
}
