import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})

export class SidebarComponent implements OnInit {
  user: any;
  menuItems = [];
  dashboard: any;
  category: string;
  setting: string;
  subadmin: string;
   role: any;
  access: any;
  cms: any;
  version: string;
  album:string
  artist:any;
  song: string;
  playlist: string;
  contact :string;
  celebrity:string;
  report:string;

  constructor() {
    this.dashboard = 'assets/icons/dash.svg',
    this.user = 'assets/icons/user.svg',
    this.category = 'assets/icons/category.svg'
    this.setting = 'assets/icons/gear.svg'
    this.subadmin = 'assets/icons/subadmin.svg'
    this.cms = 'assets/icons/CMS.svg'
    this.version = 'assets/icons/version.svg'
    this.artist = 'assets/icons/artist.svg';
    this.album = 'assets/icons/album.svg';
    this.song = 'assets/icons/song.svg'
    this.playlist = 'assets/icons/playlist.svg'
    this.contact = 'assets/icons/contactus.png'
    this.celebrity = 'assets/icons/celebrity.svg'
    this.report = 'assets/icons/report.svg'


  }
  // user:any;


  ngOnInit() {
   this.menuItems = [
      {path: '/dashboard/home', title: 'Dashboard', icon: this.dashboard, class: ''},
      {path: '/dashboard/users', title: 'Users', icon: this.user, class: ''},
      {path: '/dashboard/sub-admin', title: 'Sub Admin', icon: this.subadmin, class: ''},
      {path: '/dashboard/categories', title: 'Category', icon: this.category, class: ''},
      {path: '/dashboard/artist', title: 'Artist', icon: this.artist, class: ''},
      {path: '/dashboard/album', title: 'Album', icon: this.album, class: ''},
      {path: '/dashboard/song', title: 'Song', icon: this.song, class: ''},
      {path: '/dashboard/playlist', title: 'Playlist', icon: this.playlist, class: ''},
      {path: '/dashboard/app-version', title: 'APP Version', icon: this.version, class: ''},
      {path: '/dashboard/contact', title: 'Contact', icon: this.contact ,class: ''},
      {path: '/dashboard/celebrity', title: 'Celebrity', icon: this.celebrity ,class: ''},
      {path: '/dashboard/report', title: 'Report', icon: this.report ,class: ''},
      {path: '/dashboard/subscription', title: 'Subscription', icon: this.setting, class: ''},
      {path: '/dashboard/cms', title: 'CMS ', icon: this.cms, class: ''},





// 

     
    ];
    if (JSON.stringify(localStorage.getItem('Kandle_Admin'))) {
      this.role = JSON.parse(localStorage.getItem('Kandle_Admin'))._value.role
      this.access = JSON.parse(localStorage.getItem('Kandle_Admin'))._value.access
    }

  }

}
